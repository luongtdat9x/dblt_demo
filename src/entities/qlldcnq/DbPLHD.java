package entities.qlldcnq;

public class DbPLHD {
    public DbPLHD() {
        super();
    }
    private String id;
    private String name;
    private String idNHTM;
    private String soTien;
    private String maNHTM;
    private String soTK;
    private String tenTKN;
    private String noiMoTKN;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIdNHTM(String idNHTM) {
        this.idNHTM = idNHTM;
    }

    public String getIdNHTM() {
        return idNHTM;
    }

    public void setSoTien(String soTien) {
        this.soTien = soTien;
    }

    public String getSoTien() {
        return soTien;
    }

    public void setMaNHTM(String maNHTM) {
        this.maNHTM = maNHTM;
    }

    public String getMaNHTM() {
        return maNHTM;
    }

    public void setSoTK(String soTK) {
        this.soTK = soTK;
    }

    public String getSoTK() {
        return soTK;
    }

    public void setTenTKN(String tenTKN) {
        this.tenTKN = tenTKN;
    }

    public String getTenTKN() {
        return tenTKN;
    }

    public void setNoiMoTKN(String noiMoTKN) {
        this.noiMoTKN = noiMoTKN;
    }

    public String getNoiMoTKN() {
        return noiMoTKN;
    }
}
