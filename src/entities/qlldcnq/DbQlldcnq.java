package entities.qlldcnq;

public class DbQlldcnq {
    public DbQlldcnq() {
        super();
    }
    private String rn;
    private String id;
    private String idNHTM;
    private String idPL;
    private String soLenh;
    private String soTien;
    private String nguoiTao;
    private String ngayTao;
    private String nam;
    private String ngayThucHien;
    private String trangThai;
    private String ttBC;
    private String ngDuyet1;
    private String ngayDuyet1;
    private String ngDuyet2;
    private String ngayDuyet2;
    private String ngDuyet3;
    private String ngayDuyet3;
    private String ldTC;
    private String maNHTM;
    private String tenNHTM;
    private String duLieuKy;
    private String idTKC;
    private String soTKC;
    private String theoToTrinh;
    private String ndtt;

    public void setRn(String rn) {
        this.rn = rn;
    }

    public String getRn() {
        return rn;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setIdNHTM(String idNHTM) {
        this.idNHTM = idNHTM;
    }

    public String getIdNHTM() {
        return idNHTM;
    }

    public void setIdPL(String idPL) {
        this.idPL = idPL;
    }

    public String getIdPL() {
        return idPL;
    }

    public void setSoLenh(String soLenh) {
        this.soLenh = soLenh;
    }

    public String getSoLenh() {
        return soLenh;
    }

    public void setSoTien(String soTien) {
        this.soTien = soTien;
    }

    public String getSoTien() {
        return soTien;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayThucHien(String ngayThucHien) {
        this.ngayThucHien = ngayThucHien;
    }

    public String getNgayThucHien() {
        return ngayThucHien;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTtBC(String ttBC) {
        this.ttBC = ttBC;
    }

    public String getTtBC() {
        return ttBC;
    }

    public void setNgDuyet1(String ngDuyet1) {
        this.ngDuyet1 = ngDuyet1;
    }

    public String getNgDuyet1() {
        return ngDuyet1;
    }

    public void setNgayDuyet1(String ngayDuyet1) {
        this.ngayDuyet1 = ngayDuyet1;
    }

    public String getNgayDuyet1() {
        return ngayDuyet1;
    }

    public void setNgDuyet2(String ngDuyet2) {
        this.ngDuyet2 = ngDuyet2;
    }

    public String getNgDuyet2() {
        return ngDuyet2;
    }

    public void setNgayDuyet2(String ngayDuyet2) {
        this.ngayDuyet2 = ngayDuyet2;
    }

    public String getNgayDuyet2() {
        return ngayDuyet2;
    }

    public void setNgDuyet3(String ngDuyet3) {
        this.ngDuyet3 = ngDuyet3;
    }

    public String getNgDuyet3() {
        return ngDuyet3;
    }

    public void setNgayDuyet3(String ngayDuyet3) {
        this.ngayDuyet3 = ngayDuyet3;
    }

    public String getNgayDuyet3() {
        return ngayDuyet3;
    }

    public void setLdTC(String ldTC) {
        this.ldTC = ldTC;
    }

    public String getLdTC() {
        return ldTC;
    }

    public void setMaNHTM(String maNHTM) {
        this.maNHTM = maNHTM;
    }

    public String getMaNHTM() {
        return maNHTM;
    }

    public void setTenNHTM(String tenNHTM) {
        this.tenNHTM = tenNHTM;
    }

    public String getTenNHTM() {
        return tenNHTM;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public String getNam() {
        return nam;
    }

    public void setDuLieuKy(String duLieuKy) {
        this.duLieuKy = duLieuKy;
    }

    public String getDuLieuKy() {
        return duLieuKy;
    }

    public void setIdTKC(String idTKC) {
        this.idTKC = idTKC;
    }

    public String getIdTKC() {
        return idTKC;
    }

    public void setSoTKC(String soTKC) {
        this.soTKC = soTKC;
    }

    public String getSoTKC() {
        return soTKC;
    }

    public void setTheoToTrinh(String theoToTrinh) {
        this.theoToTrinh = theoToTrinh;
    }

    public String getTheoToTrinh() {
        return theoToTrinh;
    }

    public void setNdtt(String ndtt) {
        this.ndtt = ndtt;
    }

    public String getNdtt() {
        return ndtt;
    }
}
