package entities.qlldcnq;

public class SearchLenh {
    public SearchLenh() {
        super();
    }
    private String soLenh;
    private String nam;
    private String idNHTM;
    private String trangThai;
    private String ngayLenh1;
    private String ngayLenh2;
    private String pageIndex;
    private String rowsInPage;

    public void setSoLenh(String soLenh) {
        this.soLenh = soLenh;
    }

    public String getSoLenh() {
        return soLenh;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public String getNam() {
        return nam;
    }

    public void setIdNHTM(String idNHTM) {
        this.idNHTM = idNHTM;
    }

    public String getIdNHTM() {
        return idNHTM;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setNgayLenh1(String ngayLenh1) {
        this.ngayLenh1 = ngayLenh1;
    }

    public String getNgayLenh1() {
        return ngayLenh1;
    }

    public void setNgayLenh2(String ngayLenh2) {
        this.ngayLenh2 = ngayLenh2;
    }

    public String getNgayLenh2() {
        return ngayLenh2;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setRowsInPage(String rowsInPage) {
        this.rowsInPage = rowsInPage;
    }

    public String getRowsInPage() {
        return rowsInPage;
    }
}
