package entities.qlldcnq;

public class DbTKKBNN {
    public DbTKKBNN() {
        super();
    }
    
    private String id;
    private String soTK;
    private String chuTK;
    private String nganHang;
    private String chiNhanh;
    private String trangThai;
    private String tenDonVi;
    private String diaChi;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSoTK(String soTK) {
        this.soTK = soTK;
    }

    public String getSoTK() {
        return soTK;
    }

    public void setChuTK(String chuTK) {
        this.chuTK = chuTK;
    }

    public String getChuTK() {
        return chuTK;
    }

    public void setNganHang(String nganHang) {
        this.nganHang = nganHang;
    }

    public String getNganHang() {
        return nganHang;
    }

    public void setChiNhanh(String chiNhanh) {
        this.chiNhanh = chiNhanh;
    }

    public String getChiNhanh() {
        return chiNhanh;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTenDonVi(String tenDonVi) {
        this.tenDonVi = tenDonVi;
    }

    public String getTenDonVi() {
        return tenDonVi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getDiaChi() {
        return diaChi;
    }
}
