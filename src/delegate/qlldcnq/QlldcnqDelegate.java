package delegate.qlldcnq;

import java.util.Collection;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import dao.qlldcnq.DbQlldcnqDAO;
import entities.qlldcnq.DbQlldcnq;


public class QlldcnqDelegate {
    private SqlSession session;

    public QlldcnqDelegate(SqlSession session) {
        this.session = session;
    }

    public Collection<?> getAllLenh(Map<String, Object> parms) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getAllLenh(parms);
    }

    public Collection<?> getListNam() throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getListNam();
    }

    public Collection<?> getListNHTM() throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getListNHTM();
    }

    public Collection<?> getListTKKBNN() throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getListTKKBNN();
    }

    public Collection<?> getListPLHD() throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getListPLHD();
    }

    public DbQlldcnq getAutoID() throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return (DbQlldcnq)lenhDao.getAutoID();
    }

    public int getCountLenh(Map<String, Object> parms) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.getCountLenh(parms);
    }

    public int insertLenh(DbQlldcnq dbQlldcnq) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.insertLenh(dbQlldcnq);
    }

    public int updateLenh(DbQlldcnq dbQlldcnq) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.updateLenh(dbQlldcnq);
    }
    
    public int updateLenhTuChoi(DbQlldcnq dbQlldcnq) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.updateLenhTuChoi(dbQlldcnq);
    }

    public int updateDuyet(DbQlldcnq dbQlldcnq) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.updateDuyet(dbQlldcnq);
    }

    public int deleteLenh(int id) throws Exception {
        DbQlldcnqDAO lenhDao = new DbQlldcnqDAO(session);
        return lenhDao.deleteLenh(id);
    }
}
