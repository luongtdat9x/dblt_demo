package dao.qlldcnq;

import java.util.Collection;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import dao.AbDAO;
import entities.qlldcnq.DbQlldcnq;

public class DbQlldcnqDAO extends AbDAO {
    public DbQlldcnqDAO(SqlSession session) {
        super(session);
    }

    public Collection<?> getAllLenh(Map<String, Object> parms) throws Exception {
        return getListByCondition("DbQlldcnq.selectAllLenh", parms);
    }

    public Collection<?> getListNam() throws Exception {
        return getListByCondition("DbQlldcnq.listNam", null);
    }

    public Collection<?> getListNHTM() throws Exception {
        return getListByCondition("DbQlldcnq.listNHTM", null);
    }

    public Collection<?> getListTKKBNN() throws Exception {
        return getListByCondition("DbQlldcnq.listTKKBNN", null);
    }

    public Collection<?> getListPLHD() throws Exception {
        return getListByCondition("DbQlldcnq.selectAllPLHD", null);
    }

    public Object getAutoID() throws Exception {
        return getOneByCondition("DbQlldcnq.autoID", null);
    }

    public int getCountLenh(Map<String, Object> parms) throws Exception {
        return getCount("DbQlldcnq.selectCountLenh", parms);
    }

    public int insertLenh(DbQlldcnq dbQlldcnq) throws Exception {
        return insert("DbQlldcnq.insertLenh", dbQlldcnq);
    }

    public int updateLenh(DbQlldcnq dbQlldcnq) throws Exception {
        return update("DbQlldcnq.updateLenh", dbQlldcnq);
    }
    
    public int updateLenhTuChoi(DbQlldcnq dbQlldcnq) throws Exception {
        return update("DbQlldcnq.updateLenhTuChoi", dbQlldcnq);
    }

    public int updateDuyet(DbQlldcnq dbQlldcnq) throws Exception {
        return update("DbQlldcnq.updateDuyet", dbQlldcnq);
    }

    public int deleteLenh(int id) throws Exception {
        return delete("DbQlldcnq.deleteLenh", id);
    }

}
