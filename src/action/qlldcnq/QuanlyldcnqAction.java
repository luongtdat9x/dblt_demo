package action.qlldcnq;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

import common.AppConstants;
import common.DbltCommon;
import dao.Connect;
import action.AppAction;
import delegate.qlldcnq.QlldcnqDelegate;
import entities.Message;
import entities.qlldcnq.DbQlldcnq;
import entities.qlldcnq.SearchLenh;

public class QuanlyldcnqAction extends AppAction {
    private SearchLenh search;
    private DbQlldcnq dbLenh;
    private String typeAdd;
    private int idSelect;

    public QuanlyldcnqAction() {
        super();
    }

    public String executeAction(HttpServletRequest request,
                                HttpServletResponse response) throws Exception {

        return ActionSupport.SUCCESS;
    }

    public String list(HttpServletRequest request,
                       HttpServletResponse response) throws Exception {
        SqlSession session = Connect.getSqlSessionFactory().openSession();
        if (session == null) {
            DbltCommon.getMessageConnErr();
            return ActionSupport.SUCCESS;
        }
        try {
            QlldcnqDelegate lenhDel = new QlldcnqDelegate(session);
            int totalItems = 0;
            msg = new Message();
            Map<String, Object> objData = new HashMap<String, Object>();
            Collection objListLenh = new ArrayList();
            Map<String, Object> parms = new HashMap<String, Object>();
            if (search.getSoLenh() != null && !"".equals(search.getSoLenh())) {
                parms.put("soLenh", search.getSoLenh());
            }
            if (search.getNam() != null && !"".equals(search.getNam())) {
                parms.put("nam", search.getNam());
            }
            if (search.getIdNHTM() != null && !"".equals(search.getIdNHTM())) {
                parms.put("idNHTM", search.getIdNHTM());
            }
            if (search.getTrangThai() != null &&
                !"".equals(search.getTrangThai())) {
                parms.put("trangThai", search.getTrangThai());
            }
            if (search.getNgayLenh1() != null &&
                !"".equals(search.getNgayLenh1())) {
                parms.put("ngayLenh1", search.getNgayLenh1());
            }
            if (search.getNgayLenh2() != null &&
                !"".equals(search.getNgayLenh2())) {
                parms.put("ngayLenh2", search.getNgayLenh2());
            }
            DbltCommon common = new DbltCommon();
            int[] lastFist =
                common.paging(search.getPageIndex(), search.getRowsInPage());
            parms.put("startIndex", lastFist[0]);
            parms.put("endIndex", lastFist[1]);
            objListLenh = lenhDel.getAllLenh(parms);
            objData.put("listLenhs", objListLenh);
            objListLenh = lenhDel.getListNam();
            objData.put("listNams", objListLenh);
            objListLenh = lenhDel.getListNHTM();
            objData.put("listNHTMs", objListLenh);
            objListLenh = lenhDel.getListTKKBNN();
            objData.put("listTKKBNNs", objListLenh);
            objListLenh = lenhDel.getListPLHD();
            objData.put("phuLucs", objListLenh);
            totalItems = lenhDel.getCountLenh(parms);
            objData.put("totalItems", totalItems);

            DbQlldcnq dbLenhID = lenhDel.getAutoID();
            if (dbLenhID == null) {
                objData.put("idAuto", "1");
            } else {
                String idAuto = (Long.valueOf(dbLenhID.getId()) + 1) + "";
                objData.put("idAuto", idAuto);
            }

            msg.setErrCode("0");
            msg.setErrMessage("Truy vấn thành công!");
            msg.setObject(objData);
        } catch (Exception ex) {
            throw ex;
        } finally {
            session.close();
        }
        return ActionSupport.SUCCESS;
    }

    public String add(HttpServletRequest request,
                      HttpServletResponse response) throws Exception {
        SqlSession session =
            Connect.getSqlSessionFactory(request.getSession());
        if (session == null) {
            DbltCommon.getMessageConnErr();
            return ActionSupport.SUCCESS;
        }
        try {
            msg = new Message();
            QlldcnqDelegate lenhDel = new QlldcnqDelegate(session);
            dbLenh.setNguoiTao(request.getSession().getAttribute(AppConstants.APP_USER_ID_SESSION).toString());
            dbLenh.setTrangThai("01");
            if ("duyet".equals(typeAdd)) {
                getDbLenh().setTrangThai("03");
            }
            int rn = lenhDel.insertLenh(dbLenh);
            if (rn != 0) {
                msg.setErrCode("0");
                msg.setErrMessage("Tạo lệnh thành công!");
                if ("duyet".equals(typeAdd)) {
                    msg.setErrMessage("Trình phê duyệt thành công!");
                }
                session.commit();
            } else {
                msg.setErrCode("1");
                msg.setErrMessage("Tạo mới lệnh thất bại!");
            }
        } catch (Exception ex) {
            session.rollback();
            throw ex;

        } finally {
            session.close();
        }
        return ActionSupport.SUCCESS;
    }

    public String update(HttpServletRequest request,
                         HttpServletResponse response) throws Exception {
        SqlSession session =
            Connect.getSqlSessionFactory(request.getSession());
        if (session == null) {
            DbltCommon.getMessageConnErr();
            return ActionSupport.SUCCESS;
        }
        try {
            msg = new Message();
            QlldcnqDelegate lenhDel = new QlldcnqDelegate(session);
            dbLenh.setTrangThai("01");
            if ("duyet".equals(typeAdd)) {
                dbLenh.setTrangThai("03");
            }
            int rn = lenhDel.updateLenhTuChoi(dbLenh);
            if (rn != 0) {
                msg.setErrCode("0");
                msg.setErrMessage("Sửa lệnh thành công!");
                if ("duyet".equals(typeAdd)) {
                    msg.setErrMessage("Trình phê duyệt thành công!");
                }
                session.commit();
            } else {
                msg.setErrCode("1");
                msg.setErrMessage("Sửa lệnh thất bại!");
                if ("duyet".equals(typeAdd)) {
                    msg.setErrMessage("Trình phê duyệt thất bại!");
                }
            }
        } catch (Exception ex) {
            session.rollback();
            throw ex;

        } finally {
            session.close();
        }
        return ActionSupport.SUCCESS;
    }

    public String delete(HttpServletRequest request,
                         HttpServletResponse response) throws Exception {
        SqlSession session =
            Connect.getSqlSessionFactory(request.getSession());
        if (session == null) {
            DbltCommon.getMessageConnErr();
            return ActionSupport.SUCCESS;
        }
        try {
            msg = new Message();
            QlldcnqDelegate lenhDel = new QlldcnqDelegate(session);
            if("huy".equals(typeAdd)) {
                dbLenh.setTrangThai("08");
                int rn = lenhDel.updateLenhTuChoi(dbLenh);
                if (rn != 0) {
                    msg.setErrCode("0");
                    msg.setErrMessage("Hủy lệnh thành công!");
                    session.commit();
                } else {
                    msg.setErrCode("1");
                    msg.setErrMessage("Hủy lệnh thất bại!");
                }
            } else {
                int rn = lenhDel.deleteLenh(getIdSelect());
                if (rn != 0) {
                    msg.setErrCode("0");
                    msg.setErrMessage("Xóa lệnh thành công!");
                    session.commit();
                } else {
                    msg.setErrCode("1");
                    msg.setErrMessage("Xóa lệnh thất bại!");
                }
            } 
        } catch (Exception ex) {
            session.rollback();
            throw ex;

        } finally {
            session.close();
        }
        return ActionSupport.SUCCESS;
    }

    public void setDbLenh(DbQlldcnq dbLenh) {
        this.dbLenh = dbLenh;
    }

    public DbQlldcnq getDbLenh() {
        return dbLenh;
    }

    public void setTypeAdd(String typeAdd) {
        this.typeAdd = typeAdd;
    }

    public String getTypeAdd() {
        return typeAdd;
    }

    public void setIdSelect(int idSelect) {
        this.idSelect = idSelect;
    }

    public int getIdSelect() {
        return idSelect;
    }

    public void setSearch(SearchLenh search) {
        this.search = search;
    }

    public SearchLenh getSearch() {
        return search;
    }
}
