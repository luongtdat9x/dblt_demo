package action;


import com.opensymphony.xwork2.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import entities.Message;


public class AppAction extends ActionSupport implements ServletRequestAware,
ServletResponseAware {
	private transient HttpServletRequest request;
	private transient HttpServletResponse response;
	protected Message msg;
	private String action;

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public String execute() throws Exception {
		msg = new Message();
		response.setHeader("X-Frame-Options", "SAMEORIGIN");
		response.setHeader("X-XSS-Protection", "1; mode=block");
		response.setHeader("X-Content-Type-Options", "nosniff");
		response.setHeader("Strict-Transport-Security", "max-age=31536000");
		response.setHeader("Referrer-Policy", "origin-when-cross-origin");
		response.setHeader("Expect-CT", "max-age=7776000, enforce");
		response.setContentType("text/xml; charset=utf-8; gzip");
		try {
			String strMethodName = request.getParameter("action");
			String h = request.getParameter("h");
			if (!"login".equalsIgnoreCase(strMethodName)) {
				HttpSession ss = request.getSession();
			}
			if (strMethodName != null) {
				request.setAttribute("actionName", strMethodName);
				if (strMethodName.equalsIgnoreCase("list")) {
					return list(request, response);
				} else if (strMethodName.equalsIgnoreCase("add")) {
					return add(request, response);
				} else if (strMethodName.equalsIgnoreCase("update")) {
					return update(request, response);
				} else if (strMethodName.equalsIgnoreCase("delete")) {
					return delete(request, response);
				} else if (strMethodName.equalsIgnoreCase("view")) {
					return view(request, response);
				} else if (strMethodName.equalsIgnoreCase("preAdd")) {
					return preAdd(request, response);
				} else if (strMethodName.equalsIgnoreCase("process")) {
					return process(request, response);
				} else{
					return executeAction(request, response);
				}
			} else {
				return executeAction(request, response);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			msg.setErrCode("1");
			msg.setErrMessage(ex.getMessage());
			return SUCCESS;
		}
	}

	public String process(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public String preAdd(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	protected String executeAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return null;
	}

	public String list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public String add(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public String update(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public String view(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public String delete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return null;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	public void setMsg(Message msg) {
		this.msg = msg;
	}

	public Message getMsg() {
		return msg;
	}

}
