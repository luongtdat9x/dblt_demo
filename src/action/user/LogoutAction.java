package action.user;


import com.opensymphony.xwork2.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;

import dao.Connect;
import action.AppAction;


public class LogoutAction extends AppAction {
    private static String SUCCESS = "success";

    protected String executeAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
        SqlSession sessionsSql = Connect.getSqlSessionFactory(request.getSession());
        if (sessionsSql == null) {
            throw new Exception("Lỗi kết nối Cơ sở dữ liệu.");
        }
        try {

        } finally {
            sessionsSql.close();
        }
        return ActionSupport.SUCCESS;
    }
}
