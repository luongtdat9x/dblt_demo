package common;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;


import java.util.Properties;

import entities.Message;

public class DbltCommon {
	public DbltCommon() {
		super();
	}

	public int[] paging(String index, String maxSize) {
		int[] lastFist = new int[2];
		try {
			int last = (Integer.parseInt(index) * Integer.parseInt(maxSize)) + 1;
			int fist = last - Integer.parseInt(maxSize);
			lastFist[0] = fist;
			lastFist[1] = last;
		} catch (Exception ex) {
			lastFist[0] = 0;
			lastFist[1] = 0;
		}

		return lastFist;
	}

	public Message getMessage(int value, String errCode) {
		Message msg = new Message("0", "Th�nh c�ng");
		if (value <= 0) {
			msg = new Message(errCode, "Th?t b?i");
		}

		return msg;
	}

	public Message getMessage(int value, String errCode, String errMessage) {
		Message msg = new Message("0", "Th�nh c�ng");
		if (value <= 0) {
			msg = new Message(errCode, errMessage);
		}

		return msg;
	}

	public static Message getMessageConnErr() {
		Message msg = new Message("999999", "Kết nối CSDL thất bại");

		return msg;
	}

	public String hash(int value) {
		int num = 1234 + value + 3247 * 2;

		return num+"";
	}

	public String uocQuy(String quy, int nam) {
		String result = "";
		if ("I".equals(quy)) {
			result = "IV/" + (nam - 1);
		} else if ("II".equals(quy)) {
			result = "I/" + nam;
		} else if ("III".equals(quy)) {
			result = "II/" + nam;
		} else if ("IV".equals(quy)) {
			result = "III/" + nam;
		}

		return result;
	}

	public int thangQuy(String quy, int thang) {
		int result = 1;
		if ("I".equals(quy)) {
			result = thang;
		} else if ("II".equals(quy)) {
			result = (2 * 3) - 3 + thang;
		} else if ("III".equals(quy)) {
			result = (3 * 3) - 3 + thang;
		} else if ("IV".equals(quy)) {
			result = (4 * 3) - 3 + thang;
		}

		return result;
	}

	public int thangQuy(int quy, int thang) {
		int result = 1;
		result = (quy * 3) - 3 + thang;

		return result;
	}


	public String uocThang(int thang, int nam) {
		String result = "";
		if (thang == 1) {
			result = "12/" + (nam - 1);
		} else {
			result = (thang - 1) + "/" + nam;
		}

		return result;
	}

	//-------------get value properties---------------------


	public String getPropValues(String name) {
		String result = "";
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			// get the property value and print it out
			result = prop.getProperty(name);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (Exception ex) {
				System.out.println("Exception: " + ex);
			}
		}
		return result;
	}
	//-------------get value properties---------------------

	public Boolean StringCheck(String value) {        
		return true;
	}

	public static Boolean validString(String value, int min, int max) {
		return true;
	}

}
