//khai bao du lieu trang thai cho cbb 
var statusCBB = [];
cbb = new Object();
cbb.key = "01";
cbb.value = "Hoạt động";
statusCBB.push(cbb);
cbb1 = new Object();
cbb1.key = "02";
cbb1.value = "Ngừng hoạt động";
statusCBB.push(cbb1);

var trangthaiMB = [];
trangthaiMB.push( {
    key : "1", value : "Hoạt động"
});
trangthaiMB.push( {
    key : "0", value : "Ngừng hoạt động"
});
    function formatStringToNum(hanMucObject){
            hanMucObject.strKy_Han1 = formatCurrency(hanMucObject.strKy_Han1);
            hanMucObject.strKy_Han2 = formatCurrency(hanMucObject.strKy_Han2);
            hanMucObject.strKy_Han3 = formatCurrency(hanMucObject.strKy_Han3);
            hanMucObject.strTs_Ton_DKy = formatCurrency(hanMucObject.strTs_Ton_DKy);
            hanMucObject.strTs_Thu_TKy = formatCurrency(hanMucObject.strTs_Thu_TKy);
            hanMucObject.strTs_Chi_TKy = formatCurrency(hanMucObject.strTs_Chi_TKy);
            
            hanMucObject.strTy_Trong1 = formatCurrency(hanMucObject.strTy_Trong1);
            hanMucObject.strTy_Trong2 = formatCurrency(hanMucObject.strTy_Trong2);
            hanMucObject.strTy_Trong3 = formatCurrency(hanMucObject.strTy_Trong3);
            
            hanMucObject.strDa_Gui1 = formatCurrency(hanMucObject.strDa_Gui1);
            hanMucObject.strDa_Gui2 = formatCurrency(hanMucObject.strDa_Gui2);
            hanMucObject.strDa_Gui3 = formatCurrency(hanMucObject.strDa_Gui3);
            
            hanMucObject.strCon_Lai1 = formatCurrency(hanMucObject.strCon_Lai1);
            hanMucObject.strCon_Lai2 = formatCurrency(hanMucObject.strCon_Lai2);
            hanMucObject.strCon_Lai3 = formatCurrency(hanMucObject.strCon_Lai3);
            
            hanMucObject.strHe_Thong_TT1 = formatCurrency(hanMucObject.strHe_Thong_TT1);
            hanMucObject.strHe_Thong_TT2 = formatCurrency(hanMucObject.strHe_Thong_TT2);
            hanMucObject.strHe_Thong_TT3 = formatCurrency(hanMucObject.strHe_Thong_TT3);
            
           hanMucObject.strTs_Ton_CKy1 = formatCurrency(hanMucObject.strTs_Ton_CKy1);
            hanMucObject.strTs_Ton_CKy2 = formatCurrency(hanMucObject.strTs_Ton_CKy2);
            hanMucObject.strTs_Ton_CKy3 = formatCurrency(hanMucObject.strTs_Ton_CKy3);
            
            hanMucObject.strTs_Tien_NRoi = formatCurrency(hanMucObject.strTs_Tien_NRoi);
            hanMucObject.strTs_DinhMuc_Ton = formatCurrency(hanMucObject.strTs_DinhMuc_Ton);
            
            hanMucObject.strMin_Max1 = formatCurrency(hanMucObject.strMin_Max1).toString();
            hanMucObject.strMin_Max2 = formatCurrency(hanMucObject.strMin_Max2).toString();
            hanMucObject.strMin_Max3 = formatCurrency(hanMucObject.strMin_Max3).toString();
            hanMucObject.strTs_Ton_Uoc_Tinh= formatCurrency(hanMucObject.strTs_Ton_Uoc_Tinh);
    }
    function formatNumberToString(hanMucObject){
            hanMucObject.strKy_Han1 = formatNumberFromStr(hanMucObject.strKy_Han1);
            hanMucObject.strKy_Han2 = formatNumberFromStr(hanMucObject.strKy_Han2);
            hanMucObject.strKy_Han3 = formatNumberFromStr(hanMucObject.strKy_Han3);
            hanMucObject.strTs_Ton_DKy = formatNumberFromStr(hanMucObject.strTs_Ton_DKy);
            hanMucObject.strTs_Thu_TKy = formatNumberFromStr(hanMucObject.strTs_Thu_TKy);
            hanMucObject.strTs_Chi_TKy = formatNumberFromStr(hanMucObject.strTs_Chi_TKy);
            hanMucObject.strTs_Ton_CKy1 = formatNumberFromStr(hanMucObject.strTs_Ton_CKy1);
            hanMucObject.strTs_Ton_CKy2 = formatNumberFromStr(hanMucObject.strTs_Ton_CKy2);
            hanMucObject.strTs_Ton_CKy3 = formatNumberFromStr(hanMucObject.strTs_Ton_CKy3);
            hanMucObject.strTy_Trong1 = formatNumberFromStr(hanMucObject.strTy_Trong1);
            hanMucObject.strTy_Trong2 = formatNumberFromStr(hanMucObject.strTy_Trong2);
            hanMucObject.strTy_Trong3 = formatNumberFromStr(hanMucObject.strTy_Trong3);
            
            hanMucObject.strDa_Gui1 = formatNumberFromStr(hanMucObject.strDa_Gui1);
            hanMucObject.strDa_Gui2 = formatNumberFromStr(hanMucObject.strDa_Gui2);
            hanMucObject.strDa_Gui3 = formatNumberFromStr(hanMucObject.strDa_Gui3);
            
            hanMucObject.strCon_Lai1 = formatNumberFromStr(hanMucObject.strCon_Lai1);
            hanMucObject.strCon_Lai2 = formatNumberFromStr(hanMucObject.strCon_Lai2);
            hanMucObject.strCon_Lai3 = formatNumberFromStr(hanMucObject.strCon_Lai3);
            
            hanMucObject.strHe_Thong_TT1 = formatNumberFromStr(hanMucObject.strHe_Thong_TT1);
            hanMucObject.strHe_Thong_TT2 = formatNumberFromStr(hanMucObject.strHe_Thong_TT2);
            hanMucObject.strHe_Thong_TT3 = formatNumberFromStr(hanMucObject.strHe_Thong_TT3);
            
            hanMucObject.strTs_Tien_NRoi = formatNumberFromStr(hanMucObject.strTs_Tien_NRoi);
            hanMucObject.strTs_DinhMuc_Ton = formatNumberFromStr(hanMucObject.strTs_DinhMuc_Ton);
            hanMucObject.strTs_Ton_Uoc_Tinh= formatNumberFromStr(hanMucObject.strTs_Ton_Uoc_Tinh);
    }
var quySDBao = [];
//quySDBao.push({key:"",value:"Tất cả"});
quySDBao.push({key:"1",value:"I"});
quySDBao.push({key:"2",value:"II"});
quySDBao.push({key:"3",value:"III"});
quySDBao.push({key:"4",value:"IV"});


var mauCongVan = "<html>\n" + 
            "\n" + 
            "<head>\n" + 
            "<meta http-equiv=Content-Type content=\"text/html;\"/>\n" + 
            "<style>\n" + 
            "\n" + 
            "</style>\n" + 
            "\n" + 
            "</head>\n" + 
            "\n" + 
            "<body lang=\"EN-US\">\n" + 
            "\n" + 
            "<div class=\"WordSection1\">\n" + 
            "\n" + 
            "<div align=\"center\">\n" + 
            "\n" + 
            "<table class=TableGrid1 border=0 cellspacing=0 cellpadding=0 width=652\n" + 
            " style='width:488.85pt;border-collapse:collapse;border:none'>\n" + 
            " <thead>\n" + 
            "  <tr>\n" + 
            "   <td width=236 style='width:177.2pt;padding:0in 5.4pt 0in 5.4pt'>\n" + 
            "   <h1 align=\"center\" style='margin-top:6.0pt;margin-right:0in;margin-bottom:\n" + 
            "   0in;margin-left:0in;margin-bottom:.0001pt;text-align:center;text-indent:\n" + 
            "   0in;page-break-after:auto'><span   style='font-size:13.0pt;line-height:\n" + 
            "   150%;font-family:\"Times New Roman\",\"serif\"'>B&#7896; TÀI CHÍNH</span></h1>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:6.0pt;text-align:center'><b><span\n" + 
            "     style='font-size:13.0pt;line-height:150%'>KHO B&#7840;C NHÀ N&#431;&#7898;C</span></b></p>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:12.0pt;text-align:center'><b><span\n" + 
            "     style='font-size:13.0pt;line-height:150%'>S&#7889;:         \n" + 
            "   /KBNN-QLNQ</span></b></p>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:6.0pt;text-align:center'><b><span\n" + 
            "     style='line-height:150%'>V/v l&#7921;a ch&#7885;n danh sách NHTM d&#7921;\n" + 
            "   ki&#7871;n &#273;&#7875; g&#7917;i ti&#7873;n có k&#7923; h&#7841;n </span></b></p>\n" + 
            "   </td>\n" + 
            "   <td width=416 style='width:311.65pt;padding:0in 5.4pt 0in 5.4pt'>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:6.0pt;text-align:center'><b><span\n" + 
            "     style='font-size:13.0pt;line-height:150%'>C&#7896;NG HOÀ XÃ H&#7896;I\n" + 
            "   CH&#7910; NGH&#296;A VI&#7878;T NAM</span></b></p>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:6.0pt;text-align:center'><b><span\n" + 
            "     style='font-size:14.0pt;line-height:150%'>&#272;&#7897;c l&#7853;p -\n" + 
            "   T&#7921; do - H&#7841;nh phúc</span></b></p>\n" + 
            "   <p class=\"MsoNormal\" align=\"center\" style='margin-top:12.0pt;text-align:center'><b><i><span\n" + 
            "     style='font-size:14.0pt;line-height:150%'>Hà N&#7897;i, ngày      \n" + 
            "   tháng    n&#259;m 2019</span></i></b></p>\n" + 
            "   </td>\n" + 
            "  </tr>\n" + 
            " </thead>\n" + 
            "</table>\n" + 
            "\n" + 
            "</div>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" align=\"center\" style='margin-top:6.0pt;text-align:center'><b><span\n" + 
            "  style='font-size:14.0pt;line-height:150%'>THÔNG BÁO</span></b></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" align=\"center\" style='text-align:center'><b><span  \n" + 
            "style='font-size:14.0pt;line-height:150%'> V&#7873; vi&#7879;c l&#7921;a ch&#7885;n\n" + 
            "danh sách các NHTM d&#7921; ki&#7871;n g&#7917;i ti&#7873;n có k&#7923; h&#7841;n</span></b></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" align=\"center\" style='text-align:center'><b><i><span  \n" + 
            "style='font-size:13.0pt;line-height:150%'>&nbsp;</span></i></b></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" align=\"center\" style='text-align:center'><span  \n" + 
            "style='font-size:14.0pt;line-height:150%'>Kính g&#7917;i: ………………………….</span></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\"><span   style='font-size:13.0pt;line-height:150%'>&nbsp;</span></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" style='margin-top:6.0pt;text-indent:.5in'><span  \n" + 
            "style='font-size:14.0pt;line-height:150%'>C&#259;n c&#7913; danh sách các NHTM\n" + 
            "có ho&#7841;t &#273;&#7897;ng an toàn, &#7893;n &#273;&#7883;nh ban hành kèm\n" + 
            "theo công v&#259;n s&#7889; …. ngày …/…/…. c&#7911;a Ngân hàng Nhà n&#432;&#7899;c\n" + 
            "Vi&#7879;t Nam;</span></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" style='margin-top:6.0pt;text-indent:.5in'><span  \n" + 
            "style='font-size:14.0pt;line-height:150%'>C&#259;n c&#7913; danh sách các NHTM\n" + 
            "d&#7921; ki&#7871;n g&#7917;i ti&#7873;n có k&#7923; h&#7841;n c&#7911;a Kho b&#7841;c\n" + 
            "Nhà n&#432;&#7899;c &#273;ã &#273;&#432;&#7907;c B&#7897; Tài chính phê duy&#7879;t\n" + 
            "t&#7841;i ….. ngày …/…/….;</span></p>\n" + 
            "\n" + 
            "<p class=\"MsoNormal\" style='margin-top:6.0pt;text-indent:.5in'><span  \n" + 
            "style='font-size:14.0pt;line-height:150%'>Kho b&#7841;c Nhà n&#432;&#7899;c\n" + 
            "(KBNN) trân tr&#7885;ng thông báo Quý Ngân hàng &#273;ã &#273;&#432;&#7907;c l&#7921;a\n" + 
            "ch&#7885;n vào danh sách các NHTM d&#7921; ki&#7871;n &#273;&#7875; g&#7917;i\n" + 
            "ti&#7873;n có k&#7923; h&#7841;n c&#7911;a KBNN. KBNN &#273;ính kèm thông báo này\n" + 
            "d&#7921; th&#7843;o H&#7907;p &#273;&#7891;ng khung v&#7873; vi&#7879;c g&#7917;i\n" + 
            "ti&#7873;n có k&#7923; h&#7841;n (sau &#273;ây g&#7885;i t&#7855;t là “H&#7907;p\n" + 
            "&#273;&#7891;ng”).</span></p>\n" + 
            "\n" + 
            "<p class=MsoFooter style='margin-top:6.0pt;text-align:justify;text-indent:.5in'><span\n" + 
            "lang=NL style='letter-spacing:-.2pt'>Tên ngân hàng th&#432;&#417;ng m&#7841;i:\n" + 
            "{strTenNHTM}</span></p>\n" + 
            "\n" + 
            "<p class=MsoFooter style='margin-top:6.0pt;text-align:justify;text-indent:.5in'><span\n" + 
            "lang=NL style='letter-spacing:-.2pt'>Công v&#259;n s&#7889;: {strMaDuLieu}    \n" + 
            "Ngày {strNgayTao}</span></p>\n" + 
            "\n" + 
            "<p class=MsoFooter style='margin-top:6.0pt;text-align:justify;text-indent:.5in'>N&#259;m:\n" + 
            "{strNam}    Th&#7901;i gian k&#7871;t thúc:{strThoiGianKetThucBanChao}    </p>\n" + 
            "\n" + 
            "<p class=MsoFooter style='margin-top:6.0pt;text-align:justify;text-indent:.5in'>Ch&#7919;\n" + 
            "Kí: {strSignature}</p>\n" + 
            "\n" + 
            "</div>\n" + 
            "\n" + 
            "</body>\n" + 
            "\n" + 
            "</html>\n";


var trangThaiNQ= [];
trangThaiNQ.push({key:"",value:"Tất cả"});
trangThaiNQ.push({key:"01",value:"Dự thảo"});
trangThaiNQ.push({key:"02",value:"Đã điều chỉnh"});
trangThaiNQ.push({key:"03",value:"Chờ LĐ Phòng duyệt"});
trangThaiNQ.push({key:"04",value:"Từ chối"});
trangThaiNQ.push({key:"05",value:"LĐ phòng đã duyệt"});
trangThaiNQ.push({key:"06",value:"Chờ LĐ Cục duyệt"});
trangThaiNQ.push({key:"07",value:"LĐ Cục đã duyệt"});
trangThaiNQ.push({key:"08",value:"Đã hủy"});
//trangThaiNQ.push({key:"08",value:"LD Cục đã duyệt"});
//trangThaiNQ.push({key:"09",value:"Chờ LĐ KBNN duyệt "});
//trangThaiNQ.push({key:"10",value:"LĐ KBNN từ chối "});
//trangThaiNQ.push({key:"11",value:"LĐ KBNN đã duyệt "});
//trangThaiNQ.push({key:"12",value:"Đã xóa"});

var trangThaiNQPDCuc= [];
trangThaiNQPDCuc.push({key:"",value:"Tất cả"});
trangThaiNQPDCuc.push({key:"02",value:"Đã điều chỉnh"});
trangThaiNQPDCuc.push({key:"04",value:"Từ chối "});
trangThaiNQPDCuc.push({key:"06",value:"Chờ LĐ cục duyệt"});
trangThaiNQPDCuc.push({key:"07",value:"LĐ cục đã duyệt"});

var trangThaiNQPDPhong= [];
trangThaiNQPDPhong.push({key:"",value:"Tất cả"});
trangThaiNQPDPhong.push({key:"02",value:"Đã điều chỉnh"});
trangThaiNQPDPhong.push({key:"04",value:"Từ chối "});
trangThaiNQPDPhong.push({key:"03",value:"Chờ LĐ phòng duyệt"});
trangThaiNQPDPhong.push({key:"06",value:"LĐ phòng đã duyệt"});



var TrangThaiCT = [];
TrangThaiCT.push( {
    key : "1", value : "Hoạt động"
});
TrangThaiCT.push( {
    key : "0", value : "Ngừng hoạt động"
});
var trangThai ={};
trangThai.tuchoi="02";
trangThai.choduyet="01";
trangThai.daduyet="03";
trangThai.duthao="06";
var pheduyetMB = [];
cbb = new Object();
cbb.key = "0";
cbb.value = "Chưa duyệt";
pheduyetMB.push(cbb);
cbb1 = new Object();
cbb1.key = "1";
cbb1.value = "Đã duyệt";
pheduyetMB.push(cbb1);

var kyDBaoCBB = [];
kyDBaoCBB.push( {
    key : "NAM", value : "Năm"
});
kyDBaoCBB.push( {
    key : "QUY", value : "Quý"
});
kyDBaoCBB.push( {
    key : "THANG", value : "Tháng"
});
kyDBaoCBB.push( {
    key : "TUAN", value : "Tuần"
});

var kyDBaoKSC = [];
kyDBaoKSC.push( {
    key : "NAM", value : "Năm"
});
kyDBaoKSC.push( {
    key : "QUY", value : "Quý"
});
kyDBaoKSC.push( {
    key : "THANG", value : "Tháng"
});

var kyDBaoSGD = []; 
kyDBaoSGD.push( {
    key : "TUAN", value : "Tuần"
});

var mauBaoCaoCBB = [];
mauBaoCaoCBB.push( {
    key : "05_DBTH", value : "05/DBTH", title: "BIỂU DỰ BÁO LUỒNG TIỀN TỔNG HỢP"
});
mauBaoCaoCBB.push( {
    key : "06_DBVN", value : "06/DBVN", title: "BIỂU DỰ BÁO LUỒNG TIỀN BẰNG ĐỒNG VIỆT NAM"
});
mauBaoCaoCBB.push( {
    key : "07_DBNT", value : "07/DBNT", title: "BIỂU DỰ BÁO LUỒNG TIỀN BẰNG NGOẠI TỆ"
});

function hash(msg) {
  // returns the byte length of an utf8 string
  var s = msg.length;
  for (var i=msg.length-1; i>=0; i--) {
    var code = msg.charCodeAt(i);
    if (code > 0x7f && code <= 0x7ff) s++;
    else if (code > 0x7ff && code <= 0xffff) s+=2;
    if (code >= 0xDC00 && code <= 0xDFFF) i--; //trail surrogate
  }
  
  return randomNum(s,2);
}

var mauBaoCaoNhapCBBKSC = [];
//mauBaoCaoNhapCBBKSC.push( {
//    key : "", value : "Tất cả"
//});
mauBaoCaoNhapCBBKSC.push( {
    key : "VKSC", value : "01/DB-KSC"
}); 

var mauBaoCaoNhapCBB = [];
//mauBaoCaoNhapCBB.push( {
//    key : "", value : "Tất cả"
//});
mauBaoCaoNhapCBB.push( {
    key : "KBDP", value : "02/DB-KBĐP"
});
mauBaoCaoNhapCBB.push( {
    key : "TCT", value : "01/DB-TCT"
});
mauBaoCaoNhapCBB.push( {
    key : "TCDN", value : "08/DB-TCDN"
});
mauBaoCaoNhapCBB.push( {
    key : "TCHQ", value : "02/DB-TCHQ"
});
mauBaoCaoNhapCBB.push( {
    key : "QLN", value : "03/DB-CQLN"
});
mauBaoCaoNhapCBB.push( {
    key : "NSNN", value : "04/DB-NSNN"
});
mauBaoCaoNhapCBB.push( {
    key : "VKSC", value : "01/DB-KSC"
}); 


var mauBaoCaoNhapCBB1 = [];  
mauBaoCaoNhapCBB1.push( {
    key : "KBDP", value : "02/DB-KBĐP"
});



var mauBaoCaoThangNhapCBB = [];
mauBaoCaoThangNhapCBB.push( {
    key : "VKSC", value : "01/DB-KSC"
});
mauBaoCaoThangNhapCBB.push( {
    key : "KBDP", value : "03/DB-KBĐP"
});

var mauBaoCaoThangNhapCBBKSC = [];
mauBaoCaoThangNhapCBBKSC.push( {
    key : "VKSC", value : "01/DB-KSC"
});
 


var mauBaoCaoThangNhapCBB1 = [];
mauBaoCaoThangNhapCBB1.push( {
    key : "KBDP", value : "03/DB-KBĐP"
//      key : "KBDP_TUAN", value : "03/DB-KBĐP"

});
 
var mauBaoCaoTuanNhapCBB = []; 
mauBaoCaoTuanNhapCBB.push( {
    key : "KBDP_TUAN", value : "05/DB-KBĐP"
});
mauBaoCaoTuanNhapCBB.push( {
    key : "SGD_TUAN", value : "04/DB-SGD"
});

var mauBaoCaoTuanNhapCBB1 = []; 
mauBaoCaoTuanNhapCBB1.push( {
    key : "KBDP_TUAN", value : "05/DB-KBĐP"
});

var mauBaoCaoTuanNhapCBB2 = []; 
mauBaoCaoTuanNhapCBB2.push( {
    key : "SGD_TUAN", value : "04/DB-SGD"
});


//mauBaoCaoTuanNhapCBB.push( {
//    key : "KBNN_DP_SGD_TUAN", value : "04/DBT-KBĐP"
//});

var mauBaoCaoThangCBB = [];
mauBaoCaoThangCBB.push( {
    key : "09_DBTG", value : "09/DBTG",title:"BIỂU DỰ BÁO LUỒNG TIỀN THEO THÁNG"
});

var mauBaoCaoTuanCBB = [];
mauBaoCaoTuanCBB.push( {
    key : "10_DBT", value : "10/DBT",title:"BIỂU DỰ BÁO LUỒNG TIỀN THEO THÁNG"
});


var kyDBaoNgangCBB = [];
kyDBaoNgangCBB.push( {
    key : "NAM", value : "Năm"
});
kyDBaoNgangCBB.push( {
    key : "QUY", value : "Quý"
});

var dfTinh = "0010";

var thangDBaoCBB = [];
thangDBaoCBB.push( {
    key : "1", value : "1"
});
thangDBaoCBB.push( {
    key : "2", value : "2"
});
thangDBaoCBB.push( {
    key : "3", value : "3"
});
thangDBaoCBB.push( {
    key : "4", value : "4"
});
thangDBaoCBB.push( {
    key : "5", value : "5"
});
thangDBaoCBB.push( {
    key : "6", value : "6"
});
thangDBaoCBB.push( {
    key : "7", value : "7"
});
thangDBaoCBB.push( {
    key : "8", value : "8"
});
thangDBaoCBB.push( {
    key : "9", value : "9"
});
thangDBaoCBB.push( {
    key : "10", value : "10"
});
thangDBaoCBB.push( {
    key : "11", value : "11"
});
thangDBaoCBB.push( {
    key : "12", value : "12"
});
var thangDBao = [];
thangDBao.push( {
    key : "1", value : "01"
});
thangDBao.push( {
    key : "1", value : "02"
});
thangDBao.push( {
    key : "1", value : "03"
});
thangDBao.push( {
    key : "2", value : "04"
});
thangDBao.push( {
    key : "2", value : "05"
});
thangDBao.push( {
    key : "2", value : "06"
});
thangDBao.push( {
    key : "3", value : "07"
});
thangDBao.push( {
    key : "3", value : "08"
});
thangDBao.push( {
    key : "3", value : "09"
});
thangDBao.push( {
    key : "4", value : "10"
});
thangDBao.push( {
    key : "4", value : "11"
});
thangDBao.push( {
    key : "4", value : "12"
});
var thangSDBaoCBB = [];
thangSDBaoCBB.push( {
    key : "", value : "Tất cả"
});
thangSDBaoCBB.push( {
    key : "1", value : "1"
});
thangSDBaoCBB.push( {
    key : "2", value : "2"
});
thangSDBaoCBB.push( {
    key : "3", value : "3"
});
thangSDBaoCBB.push( {
    key : "4", value : "4"
});
thangSDBaoCBB.push( {
    key : "5", value : "5"
});
thangSDBaoCBB.push( {
    key : "6", value : "6"
});
thangSDBaoCBB.push( {
    key : "7", value : "7"
});
thangSDBaoCBB.push( {
    key : "8", value : "8"
});
thangSDBaoCBB.push( {
    key : "9", value : "9"
});
thangSDBaoCBB.push( {
    key : "10", value : "10"
});
thangSDBaoCBB.push( {
    key : "11", value : "11"
});
thangSDBaoCBB.push( {
    key : "12", value : "12"
});

var quy = [];
quy.push( {
    key : "1", value : "I"
});
quy.push( {
    key : "2", value : "II"
});
quy.push( {
    key : "3", value : "III"
});
quy.push( {
    key : "4", value : "IV"
});

var trangThaiGuiNhan = [];
trangThaiGuiNhan.push( {
    key : "03", value : "Đã gửi"
});
trangThaiGuiNhan.push( {
    key : "01", value : "Chưa gửi"
});

var namDBaoCBB = [];
cbb = new Object();
cbb.key = "2018";
cbb.value = "2018";
namDBaoCBB.push(cbb);
cbb1 = new Object();
cbb1.key = "2019";
cbb1.value = "2019";
namDBaoCBB.push(cbb1);

var quyDBaoCBB = [];
quyDBaoCBB.push( {
    key : "I", value : "I"
});
quyDBaoCBB.push( {
    key : "II", value : "II"
});
quyDBaoCBB.push( {
    key : "III", value : "III"
});
quyDBaoCBB.push( {
    key : "IV", value : "IV"
});

var quySDBaoCBB = [];
quySDBaoCBB.push( {
    key : "", value : "Tất cả"
});
quySDBaoCBB.push( {
    key : "I", value : "I"
});
quySDBaoCBB.push( {
    key : "II", value : "II"
});
quySDBaoCBB.push( {
    key : "III", value : "III"
});
quySDBaoCBB.push( {
    key : "IV", value : "IV"
});

var trangThaiCbbDuyet = [];
trangThaiCbbDuyet.push( {
    key : "", value : "Tất cả"
});
trangThaiCbbDuyet.push( {
    key : "06", value : "Dự thảo"
});
trangThaiCbbDuyet.push( {
    key : "01", value : "Chờ duyệt"
});
trangThaiCbbDuyet.push( {
    key : "02", value : "Từ chối"
});
trangThaiCbbDuyet.push( {
    key : "03", value : "Đã duyệt"
});
trangThaiCbbDuyet.push( {
    key : "04", value : "Đã điều chỉnh"
});
trangThaiCbbDuyet.push( {
    key : "05", value : "Hủy"
});

var trangThaiCbbKhongDuyet = [];
trangThaiCbbKhongDuyet.push( {
    key : "", value : "Tất cả"
});
trangThaiCbbKhongDuyet.push( {
    key : "03", value : "Hoàn thành"
});
trangThaiCbbKhongDuyet.push( {
    key : "04", value : "Đã điều chỉnh"
});
trangThaiCbbKhongDuyet.push( {
    key : "05", value : "Hủy"
});
var trangThaiCbbDuyet1 = [];
trangThaiCbbDuyet1.push( {
    key : "", value : "Tất cả"
});
trangThaiCbbDuyet1.push( {
    key : "06", value : "Dự thảo"
});
trangThaiCbbDuyet1.push( {
    key : "01", value : "Chờ duyệt"
});
trangThaiCbbDuyet1.push( {
    key : "02", value : "Từ chối"
});
trangThaiCbbDuyet1.push( {
    key : "03", value : "Đã duyệt"
});
trangThaiCbbDuyet1.push( {
    key : "04", value : "Đã điều chỉnh"
});
trangThaiCbbDuyet1.push( {
    key : "05", value : "Hủy"
});

var trangThaiCbbKhongDuyet1 = [];
trangThaiCbbKhongDuyet1.push( {
    key : "", value : "Tất cả"
});
trangThaiCbbKhongDuyet1.push( {
    key : "03", value : "Hoàn thành"
});
trangThaiCbbKhongDuyet1.push( {
    key : "04", value : "Đã điều chỉnh"
});
trangThaiCbbKhongDuyet1.push( {
    key : "05", value : "Hủy"
});
var trangThaiCbb = [];
trangThaiCbb.push( {
    key : "all", value : "Tất cả"
});
trangThaiCbb.push( {
    key : "06", value : "Dự thảo"
});
trangThaiCbb.push( {
    key : "01", value : "Chờ duyệt"
});
trangThaiCbb.push( {
    key : "02", value : "Từ chối"
});
trangThaiCbb.push( {
    key : "03", value : "Đã duyệt"
});
trangThaiCbb.push( {
    key : "03", value : "Hoàn thành"
});
trangThaiCbb.push( {
    key : "04", value : "Đã điều chỉnh"
});



var trangThaiCBBTH = [];
trangThaiCBBTH.push( {
    key : "", value : "Tất cả"
});
trangThaiCBBTH.push( {
    key : "01", value : "Dự thảo"
});
trangThaiCBBTH.push( {
    key : "02", value : "Chờ LĐ phòng duyệt"
});
trangThaiCBBTH.push( {
    key : "04", value : "Chờ LĐ cục duyệt"
});
trangThaiCBBTH.push( {
    key : "05", value : "Đã duyệt"
});
trangThaiCBBTH.push( {
    key : "06", value : "Đã điều chỉnh"
});
trangThaiCBBTH.push( {
    key : "03", value : "Từ chối"
});

var trangThai = {
 
    choduyet : "01", tuchoi : "02", daduyet : "03", dadieuchinh : "04", duthao : "06"
 
}

var trangThaiTH = {
 
    duthao : "01", choldphongduyet : "02", tuchoi : "03", choldcucduyet : "04", daduyet : "05", dadieuchinh : "06", huy : "07"
 
}

var actionReport = {
 
    print : "PRINT", excel : "EXCEL", word : "WORD"
 
}

var ctietTuan = {
    id : "", dlieu_dbao_id : "", ma_ctieu : "", ngay_db : "", gia_tri : ""
}

//var trangThaiDuyetCbb = [];
//trangThaiDuyetCbb.push({key:"01",value:"Chờ duyệt"});
//trangThaiDuyetCbb.push({key:"03",value:"Đã duyệt"});
var trangThaiDuyetCbb = [];
trangThaiDuyetCbb.push( {
    key : "all", value : "Tất cả"
});

trangThaiDuyetCbb.push( {
    key : "01", value : "Chờ duyệt"
});
trangThaiDuyetCbb.push( {
    key : "02", value : "Từ chối"
});
trangThaiDuyetCbb.push( {
    key : "03", value : "Đã duyệt"
});
trangThaiDuyetCbb.push( {
    key : "04", value : "Đã điều chỉnh"
});

//trangThaiDuyetCbb.push( {
//    key : "05", value : "Gỡ duyệt"
//});
//trangThaiDuyetCbb.push( {
//    key : "06", value : "Hoàn thành"
//});


var duyetCBB = [];
duyetCBB.push( {
    key : "", value : "Tất cả"
});
duyetCBB.push( {
    key : "01", value : "Chờ duyệt"
});
duyetCBB.push( {
    key : "02", value : "Từ chối"
});
duyetCBB.push( {
    key : "03", value : "Đã duyệt"
});
duyetCBB.push( {
    key : "04", value : "Đã điều chỉnh"
}); 
duyetCBB.push( {
    key : "03", value : "Hoàn thành"
});

var trangThaiTongHopCbb = [];
trangThaiTongHopCbb.push( {
    key : "03", value : "Đã duyệt"
});

var donviKBCbb = [];
donviKBCbb.push( {
    key : "DONVIKBNN", value : "Đơn vị KBNN"
});
donviKBCbb.push( {
    key : "0003", value : "Sở giao dịch KBNN"
});

SOGDKBNN = '0003';

function checkError(error) {
    jQuery('#content_body').hideLoading();
    if (error == "login") {
        window.location.href = error;
    }
    else if (error == "permission") {
        alert("Không có quyền");
    }
    else {
        alert("Không thành công");
    }
}

function setDlieuDBaoCtietQuayDoc(kyDBao, dbTemplates, dateOfMonth,sgd) {
    var dbDlieuDbaoCtietQuayDocs = [];
    var ky = "";
    var cnt = 0;
    var chimuc = "";
    if (kyDBao == 'TUAN') {
        cnt = dateOfMonth.length;
    }
    else if (kyDBao == 'THANG') {
        chimuc = "Ngày ";
        ky = "ngay";
        cnt = dateOfMonth;
    }
    else if (kyDBao == 'QUY') {
        chimuc = "Tháng ";
        ky = "thang";
        cnt = 3;
    }
    else if (kyDBao == 'NAM') {
        chimuc = "Quý ";
        ky = "quy";
        cnt = 4;
    }
    //uoc
    var dlieuDBaoCT = {
    };
    dlieuDBaoCT.chi_muc = "Ước";
    dlieuDBaoCT["uoc"] = 0;
    dlieuDBaoCT['stt'] = "UOC";
    for (var i = 0;i < dbTemplates.length;i++) {
        if(sgd==true){
          dlieuDBaoCT[dbTemplates[i].ma_ctieu] = 0;
        }else{
          dlieuDBaoCT[dbTemplates[i].ten_ctieu] = 0;
        }
    }
    dbDlieuDbaoCtietQuayDocs.push(dlieuDBaoCT);

    if (kyDBao == 'TUAN') {
        for (var j = 0;j < cnt;j++) {
            dlieuDBaoCT = {
            };
            dlieuDBaoCT.chi_muc = getFormattedDate(dateOfMonth[j]);
            for (var i = 0;i < dbTemplates.length;i++) {
                if(sgd==true){
                  dlieuDBaoCT[dbTemplates[i].ma_ctieu] = 0;
                }else{
                  dlieuDBaoCT[dbTemplates[i].ten_ctieu] = 0;
                }
                dlieuDBaoCT['stt'] = j + 1;
            }
            dbDlieuDbaoCtietQuayDocs.push(dlieuDBaoCT);
        }
    }
    else {
        for (var j = 1;j <= cnt;j++) {
            dlieuDBaoCT = {
            };
            dlieuDBaoCT.chi_muc = chimuc + j;
            for (var i = 0;i < dbTemplates.length;i++) {
                dlieuDBaoCT[dbTemplates[i].ten_ctieu] = 0;
                dlieuDBaoCT['stt'] = ky + j;
            }
            dbDlieuDbaoCtietQuayDocs.push(dlieuDBaoCT);
        }
    }

    dlieuDBaoCT = {
    };
    dlieuDBaoCT.chi_muc = "Tổng";
    dlieuDBaoCT["tong"] = 0;
    for (var i = 0;i < dbTemplates.length;i++) {
        if(sgd==true){
          dlieuDBaoCT[dbTemplates[i].ma_ctieu] = 0;;
        }else{
          dlieuDBaoCT[dbTemplates[i].ten_ctieu] = 0;
        }
    }
    dlieuDBaoCT['stt'] = "TONG";
    dbDlieuDbaoCtietQuayDocs.push(dlieuDBaoCT);

    return dbDlieuDbaoCtietQuayDocs;
}

function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return day + '/' + month + '/' + year;
}

function setDlieuDBaoQuayNgangCtiet(kyDBao, dbTemplates, dateOfMonth) {
    var dbDlieuDbaoCtietQuayNgangs = [];
    var ky = "";
    var cnt = 0;
    var chimuc = "";
    if (kyDBao == 'THANG') {
        for (var j = 1;j <= dateOfMonth;j++) {
            chimuc = "Ngày ";
            ky = "ngay";
            cnt = dateOfMonth;
        }
    }
    else if (kyDBao == 'QUY') {
        chimuc = "Tháng ";
        ky = "thang";
        cnt = 3;
    }
    else if (kyDBao == 'NAM') {
        chimuc = "Quý ";
        ky = "quy";
        cnt = 4;
    }
    for (var i = 0;i < dbTemplates.length;i++) {
        var dlieuDBaoCT = {
        };
     //   dlieuDBaoCT.loai_tien = "";   // dung
        dlieuDBaoCT.chi_muc = i;
        dlieuDBaoCT.uoc = 0;
        dlieuDBaoCT.tong = 0;
        dlieuDBaoCT.ma_ctieu = dbTemplates[i].ma_ctieu;
        dlieuDBaoCT.ten_ctieu = dbTemplates[i].ten_ctieu;
        for (var j = 1;j <= cnt;j++) {
            var field = ky + j;
            dlieuDBaoCT[field] = 0;
        }
        dbDlieuDbaoCtietQuayNgangs.push(dlieuDBaoCT);
    }

    return dbDlieuDbaoCtietQuayNgangs;
}

var dbDlieuDbaoNamCtiet = {
    chi_muc : "", chi_tieu : "", ten_ctieu : "", uoc : 0, quy1 : 0, quy2 : 0, quy3 : 0, quy4 : 0, tong : 0, disabled : false
}

var dbDlieuDbaoQuyCtiet = {
    chi_muc : "", chi_tieu : "", ten_ctieu : "", uoc : 0, thang1 : 0, thang2 : 0, thang3 : 0, tong : 0, disabled : false
}

var dbDlieuDbaoThangCtiet = {
    chi_muc : "", chi_tieu : "", ten_ctieu : "", uoc : 0, ngay1 : 0, ngay2 : 0, ngay3 : 0, ngay4 : 0, ngay5 : 0, ngay6 : 0, ngay7 : 0, ngay8 : 0, ngay9 : 0, ngay10 : 0, ngay11 : 0, ngay12 : 0, ngay13 : 0, ngay14 : 0, ngay15 : 0, ngay16 : 0, ngay17 : 0, ngay18 : 0, ngay19 : 0, ngay20 : 0, ngay21 : 0, ngay22 : 0, ngay23 : 0, ngay24 : 0, ngay25 : 0, ngay26 : 0, ngay27 : 0, ngay28 : 0, ngay29 : 0, ngay30 : 0, ngay31 : 0, ngay32 : 0, tong : 0, disabled : false
}

function formatCurrency(value) {
    if (value == null){
      return 0;
    }
    try {
        var str = '0.0';
        if(value=='-'){
          str='0';
        }else{
        str = value.replace(/\./g, '');
        str = str.replace(/\,/g, '.');
        str = str.substr(0,1)=='('? '-'+ str.substr(1,str.length-2):str;
        }
        return str;
    }
    catch (ex) {
        return value;
    }
}
function formatNumOnfocus(yourNumber){
    if(yourNumber==null || yourNumber == '') return '';
    try {
        var str = '0.0';
        str = yourNumber.substr(0,1)=='('? '-'+ yourNumber.substr(1,yourNumber.length-2):yourNumber;
        return str;
    }
    catch (ex) {
        return yourNumber;
    }
}
function formatNumberBlur(obj){
   var yourNumber = obj.value;
      if(yourNumber==null || yourNumber == ''){ 
         return 0;
      }else{
          var str = yourNumber.substr(0,1)=='-'?true:false;
          if(str) {
              if((yourNumber.length==2 &&Number(yourNumber.substr(1,1))==0)||yourNumber.length==1){
                obj.value='0';
              }else{
                var str1 = yourNumber.substr(1).replace(/[^0-9,]/g, '').replace(',','.').toString();
                if(str1 ==0){
                obj.value ='0';
                }else{
                obj.value ='(' +formatNumberFromStr(str1)+ ')';
                }
              }
          } else{ 
            str = yourNumber.substr(0,1);
            if(str == '.' || str ==','){
                var strNumber= yourNumber.toString().replace(/[^0-9,]/g, '').replace(',','.');
                if(Number(strNumber)==0){
                obj.value = '0';
                }else{
                obj.value=formatNumberFromStr(Number(strNumber));
                }
            }else{
              var strNumber1= yourNumber.toString().replace(/[^0-9,]/g, '').replace(',','.');
              var str3 = strNumber1.substr(0,1)=='0'?true:false;
              if(str3){
                obj.value = formatNumberFromStr(Number(strNumber1));
              }else{
                obj.value = formatNumberFromStr(Number(strNumber1));
              }
            }
          };
      }
}
function formatNumberFromStr(yourNumber) {
    if(yourNumber==null || yourNumber == '') return '0';
    var components = yourNumber.toString().replace(/\./g, ',').split(",");
    if(components[0]==""||components[0]==" "){
      components[0]=0;
    }
    if (components.length === 1)
        components[0] = yourNumber;
    components[0] = components[0].toString().replace(/(?!-)[^0-9.]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    if (components.length === 2)
        components[1] = components[1].toString().replace(/(?!-)[^0-9.]/g, "").substr(0, 2);
    var numStr = components.join(",").substr(0,1)=='-'?'(' +components.join(",").substr(1) + ')':components.join(",");
    return numStr;
}

function setDataFormatMoney(dt, kyDb, dateOfMonth) {
    for (var i = 0;i < dt.length;i++) {       
        if (kyDb == "NAM") {
             dt[i].uoc = formatNumberFromStr(dt[i].uoc);
             dt[i].tong = formatNumberFromStr(dt[i].tong);
             dt[i].quy1 = formatNumberFromStr(dt[i].quy1);
             dt[i].quy2 = formatNumberFromStr(dt[i].quy2);
             dt[i].quy3 = formatNumberFromStr(dt[i].quy3);
             dt[i].quy4 = formatNumberFromStr(dt[i].quy4);
            if(dt[i].dbnam != null){
              dt[i].dbnam = formatNumberFromStr(dt[i].dbnam);
            }
            if(dt[i].sso_tuyetdoi != null){
              dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            }
            if(dt[i].sso_phantram != null ){
              dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
            }
        }
        else if (kyDb == "QUY") {
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            if(dt[i].dbquy != null){
              dt[i].dbquy = formatNumberFromStr(dt[i].dbquy);
            }
            if(dt[i].sso_tuyetdoi != null){
              dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            }
            if( dt[i].sso_phantram != null){
              dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
            }
        }
        else if (kyDb == "THANG") {
            for (var j = 1;j <= dateOfMonth;j++) {
                dt[i]['ngay' + j] = formatNumberFromStr(dt[i]['ngay' + j]);
            }
             if( dt[i].uoc != null){
              dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            }else{
              dt[i].uoc = 0; 
            }
            if( dt[i].tong != null){
              dt[i].tong = formatNumberFromStr(dt[i].tong);
            }else{
              dt[i].tong = 0;
            }
        }
        else if (kyDb == "TUAN") {
            dt[i].gia_tri = formatNumberFromStr(dt[i].gia_tri);
        }else if(kyDb == "NAM_THANG"){
            if( dt[i].uoc != null){
              dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            }else{
              dt[i].uoc = 0; 
            }
            if( dt[i].tong != null){
              dt[i].tong = formatNumberFromStr(dt[i].tong);
            }else{
              dt[i].tong = 0;
            }
            if(dt[i].thang1 != null){
              dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            }else{
              dt[i].thang1 = 0;
            }
            if( dt[i].thang2 != null){
              dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            }else{
              dt[i].thang2 = 0;
            }             
            if( dt[i].thang3 != null){
              dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            }else{
              dt[i].thang3 = 0;
            }
            if(dt[i].thang4 != null){
              dt[i].thang4 = formatNumberFromStr(dt[i].thang4);
            }else{
              dt[i].thang4 = 0;
            }
            if( dt[i].thang5 != null){
              dt[i].thang5 = formatNumberFromStr(dt[i].thang5);
            }else{
              dt[i].thang5 = 0;
            }             
            if( dt[i].thang6 != null){
              dt[i].thang6 = formatNumberFromStr(dt[i].thang6);
            }else{
              dt[i].thang6 = 0;
            }
            if(dt[i].thang7 != null){
              dt[i].thang7 = formatNumberFromStr(dt[i].thang7);
            }else{
              dt[i].thang7 = 0;
            }
            if( dt[i].thang8 != null){
              dt[i].thang8 = formatNumberFromStr(dt[i].thang8);
            }else{
              dt[i].thang8 = 0;
            }             
            if( dt[i].thang9 != null){
              dt[i].thang9 = formatNumberFromStr(dt[i].thang9);
            }else{
              dt[i].thang9 = 0;
            }
            if(dt[i].thang10 != null){
              dt[i].thang10 = formatNumberFromStr(dt[i].thang10);
            }else{
              dt[i].thang10 = 0;
            }
            if( dt[i].thang11 != null){
              dt[i].thang11 = formatNumberFromStr(dt[i].thang11);
            }else{
              dt[i].thang11 = 0;
            }             
            if( dt[i].thang12 != null){
              dt[i].thang12 = formatNumberFromStr(dt[i].thang12);
            }else{
              dt[i].thang12 = 0;
            }
            if(dt[i].dbquy != null){
              dt[i].dbquy = formatNumberFromStr(dt[i].dbquy);
            }else{
              dt[i].dbquy = 0;
            }
            if(dt[i].sso_tuyetdoi != null){
              dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            }else{
              dt[i].sso_tuyetdoi = 0;
            }
            if( dt[i].sso_phantram != null){
              dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
            }else{
              dt[i].sso_phantram = 0;
            }
        }
    }
    return dt;
}

function setDataFormatCurrency(dt, kyDb, dateOfMonth) {
    //console.log(JSON.stringify(dt));
    for (var i = 0;i < dt.length;i++) {       
        if (kyDb == "NAM") {
            dt[i].uoc = formatCurrency(dt[i].uoc);
            dt[i].tong = formatCurrency(dt[i].tong);
            dt[i].quy1 = formatCurrency(dt[i].quy1);
            dt[i].quy2 = formatCurrency(dt[i].quy2);
            dt[i].quy3 = formatCurrency(dt[i].quy3);
            dt[i].quy4 = formatCurrency(dt[i].quy4);
        }
        else if (kyDb == "QUY") {
            dt[i].uoc = formatCurrency(dt[i].uoc);
            dt[i].tong = formatCurrency(dt[i].tong);
            dt[i].thang1 = formatCurrency(dt[i].thang1);
            dt[i].thang2 = formatCurrency(dt[i].thang2);
            dt[i].thang3 = formatCurrency(dt[i].thang3);
        }
        else if (kyDb == "THANG") {
            for (var j = 1;j <= dateOfMonth;j++) {
                dt[i]['ngay' + j] = formatCurrency(dt[i]['ngay' + j]);
            }
        }
        else if (kyDb == "TUAN") {
            dt[i].gia_tri = formatCurrency(dt[i].gia_tri);
        }
    }

    return dt;
}
function formatNumberForExp(yourNumber){
    var str = '0';
    try {       
        str = yourNumber.substr(0,1)=='('? '-'+ yourNumber.substr(1,yourNumber.length-2).replace(/\./g, '').replace(/\,/g, '.'):yourNumber.toString().replace(/\./g, '').replace(/\,/g, '.');
        return formatNumberFromStr(toNumberStr(str).toString());
    }
    catch (ex) {
        return yourNumber;
    }
    
}
function formatNumbertoString(dt, kyDb) {
    for (var i = 0;i < dt.length;i++) {       
        if (kyDb == "NAM") {
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);  
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].quy1 = formatNumberFromStr(dt[i].quy1);
            dt[i].quy2 = formatNumberFromStr(dt[i].quy2);
            dt[i].quy3 = formatNumberFromStr(dt[i].quy3);
            dt[i].quy4 = formatNumberFromStr(dt[i].quy4);
            dt[i].dbnam = formatNumberFromStr(dt[i].dbnam);
            dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
        }
        else if (kyDb == "QUY") {
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            // cac thuoc tinh cho capnhat
            dt[i].dbquy = formatNumberFromStr(dt[i].dbquy);
            dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
            dt[i].chenh_lech = formatNumberFromStr(dt[i].chenh_lech);
        }
        else if (kyDb == "THANG" || kyDb == "TUAN") {
            for (var j = 0;j < dt[i].listDetailThang.length;j++) {
                dt[i].listDetailThang[j].gia_tri = formatNumberFromStr(dt[i].listDetailThang[j].gia_tri);
            }
        }else if(kyDb == "NAM_THANG"){
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            dt[i].thang4 = formatNumberFromStr(dt[i].thang4);
            dt[i].thang5 = formatNumberFromStr(dt[i].thang5);
            dt[i].thang6 = formatNumberFromStr(dt[i].thang6);
            dt[i].thang7 = formatNumberFromStr(dt[i].thang7);
            dt[i].thang8 = formatNumberFromStr(dt[i].thang8);
            dt[i].thang9 = formatNumberFromStr(dt[i].thang9);
            dt[i].thang10 = formatNumberFromStr(dt[i].thang10);
            dt[i].thang11 = formatNumberFromStr(dt[i].thang11);
            dt[i].thang12 = formatNumberFromStr(dt[i].thang12);
            dt[i].dbquy = formatNumberFromStr(dt[i].dbquy);
            dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
        }    
    }
    return dt;
}
function formatNumbertoStringLichSu(dt, kyDb) {
    for (var i = 0;i < dt.length;i++) {       
        if (kyDb == "NAM") {
            if(dt[i].uoc!=''&&dt[i].uoc!=null){
              dt[i].uoc = formatNumberFromStr(dt[i].uoc.split("#")[0])+"#"+formatNumberFromStr(dt[i].uoc.split("#")[1]);  
            }
            if(dt[i].tong!=''&&dt[i].tong!=null){
              dt[i].tong = formatNumberFromStr(dt[i].tong.split("#")[0])+"#"+formatNumberFromStr(dt[i].tong.split("#")[1]);  
            }
            if(dt[i].quy1!=''&&dt[i].quy1!=null){
              dt[i].quy1 = formatNumberFromStr(dt[i].quy1.split("#")[0])+"#"+formatNumberFromStr(dt[i].quy1.split("#")[1]);  
            }
            if(dt[i].quy2!=''&&dt[i].quy2!=null){
              dt[i].quy2 = formatNumberFromStr(dt[i].quy2.split("#")[0])+"#"+formatNumberFromStr(dt[i].quy2.split("#")[1]); 
            }
            if(dt[i].quy3!=''&&dt[i].quy3!=null){
              dt[i].quy3 = formatNumberFromStr(dt[i].quy3.split("#")[0])+"#"+formatNumberFromStr(dt[i].quy3.split("#")[1]); 
            }
            if(dt[i].quy4!=''&&dt[i].quy4!=null){
              dt[i].quy4 = formatNumberFromStr(dt[i].quy4.split("#")[0])+"#"+formatNumberFromStr(dt[i].quy4.split("#")[1]);
            }
            if(dt[i].dbnam!=''&&dt[i].dbnam!=null){
              dt[i].dbnam = formatNumberFromStr(dt[i].dbnam.split("#")[0])+"#"+formatNumberFromStr(dt[i].dbnam.split("#")[1]);
            }
            if(dt[i].sso_tuyetdoi!=''&&dt[i].sso_tuyetdoi!=null){
              dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi.split("#")[0])+"#"+formatNumberFromStr(dt[i].sso_tuyetdoi.split("#")[1]);
            }
            if(dt[i].sso_phantram!=''&&dt[i].sso_phantram!=null){
              dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram.split("#")[0])+"#"+formatNumberFromStr(dt[i].sso_phantram.split("#")[1]);
            }
        }
        else if (kyDb == "QUY") {
            if(dt[i].uoc!=''&&dt[i].uoc!=null){
              dt[i].uoc = formatNumberFromStr(dt[i].uoc.split("#")[0])+"#"+formatNumberFromStr(dt[i].uoc.split("#")[1]);  
            }
            if(dt[i].tong!=''&&dt[i].tong!=null){
              dt[i].tong = formatNumberFromStr(dt[i].tong.split("#")[0])+"#"+formatNumberFromStr(dt[i].tong.split("#")[1]);  
            }
            if(dt[i].thang1!=''&&dt[i].thang1!=null){
              dt[i].thang1 = formatNumberFromStr(dt[i].thang1.split("#")[0])+"#"+formatNumberFromStr(dt[i].thang1.split("#")[1]);  
            }
            if(dt[i].thang2!=''&&dt[i].thang2!=null){
              dt[i].thang2 = formatNumberFromStr(dt[i].thang2.split("#")[0])+"#"+formatNumberFromStr(dt[i].thang2.split("#")[1]);  
            }
            if(dt[i].thang3!=''&&dt[i].thang3!=null){
              dt[i].thang3 = formatNumberFromStr(dt[i].thang3.split("#")[0])+"#"+formatNumberFromStr(dt[i].thang3.split("#")[1]);  
            }
            // cac thuoc tinh cho capnhat
            if(dt[i].dbquy!=''&&dt[i].dbquy!=null){
              dt[i].dbquy = formatNumberFromStr(dt[i].dbquy.split("#")[0])+"#"+formatNumberFromStr(dt[i].dbquy.split("#")[1]);
            }
            if(dt[i].sso_tuyetdoi!=''&&dt[i].sso_tuyetdoi!=null){
              dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi.split("#")[0])+"#"+formatNumberFromStr(dt[i].sso_tuyetdoi.split("#")[1]);
            }
            if(dt[i].sso_phantram!=''&&dt[i].sso_phantram!=null){
              dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram.split("#")[0])+"#"+formatNumberFromStr(dt[i].sso_phantram.split("#")[1]);
            }
            if(dt[i].chenh_lech!=''&&dt[i].chenh_lech!=null){
              dt[i].chenh_lech = formatNumberFromStr(dt[i].chenh_lech.split("#")[0])+"#"+formatNumberFromStr(dt[i].chenh_lech.split("#")[1]);
            }
        }
        else if (kyDb == "THANG" || kyDb == "TUAN") {
            for (var j = 0;j < dt[i].listDetailThang.length;j++) {
//                dt[i].listDetailThang[j].gia_tri = formatNumberFromStr(dt[i].listDetailThang[j].gia_tri);
                if(dt[i].listDetailThang[j].gia_tri!=''&&dt[i].listDetailThang[j].gia_tri!=null){
                    dt[i].listDetailThang[j].gia_tri = formatNumberFromStr(dt[i].listDetailThang[j].gia_tri.split("#")[0])+"#"+formatNumberFromStr(dt[i].listDetailThang[j].gia_tri.split("#")[1]);
                } 
            }
        }else if(kyDb == "NAM_THANG"){
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            dt[i].thang4 = formatNumberFromStr(dt[i].thang4);
            dt[i].thang5 = formatNumberFromStr(dt[i].thang5);
            dt[i].thang6 = formatNumberFromStr(dt[i].thang6);
            dt[i].thang7 = formatNumberFromStr(dt[i].thang7);
            dt[i].thang8 = formatNumberFromStr(dt[i].thang8);
            dt[i].thang9 = formatNumberFromStr(dt[i].thang9);
            dt[i].thang10 = formatNumberFromStr(dt[i].thang10);
            dt[i].thang11 = formatNumberFromStr(dt[i].thang11);
            dt[i].thang12 = formatNumberFromStr(dt[i].thang12);
            dt[i].dbquy = formatNumberFromStr(dt[i].dbquy);
            dt[i].sso_tuyetdoi = formatNumberFromStr(dt[i].sso_tuyetdoi);
            dt[i].sso_phantram = formatNumberFromStr(dt[i].sso_phantram);
        }    
    }
    return dt;
}
function formatNumbertoStringDoc(detail) {
  for (var i = 0;i < detail.length;i++) { 
      for (var j = 0;j < detail[i].listCTieu.length;j++) {
            detail[i].listCTieu[j].gia_tri = formatNumberFromStr(detail[i].listCTieu[j].gia_tri);
      }
  }
  return detail;
}
function formatStringtoNumberDoc(detail) {
    for (var i = 0;i < detail.length;i++) { 
        for (var j = 0;j < detail[i].listCTieu.length;j++) {
            detail[i].listCTieu[j].gia_tri = formatCurrency(detail[i].listCTieu[j].gia_tri);
        }
    }
    return detail;
}
function formatStringtoNumber(detail, kyDb) {
    //console.log(JSON.stringify(dt));
    for (var i = 0;i < detail.length;i++) {        
        if (kyDb == "NAM") {
            detail[i].uoc = formatCurrency(detail[i].uoc);
            detail[i].tong = formatCurrency(detail[i].tong);
            detail[i].quy1 = formatCurrency(detail[i].quy1);
            detail[i].quy2 = formatCurrency(detail[i].quy2);
            detail[i].quy3 = formatCurrency(detail[i].quy3);
            detail[i].quy4 = formatCurrency(detail[i].quy4);
            if(detail[i].dbnam!=null){
                detail[i].dbnam = formatCurrency(detail[i].dbnam);
                detail[i].sso_phantram = formatCurrency(detail[i].sso_phantram);
                detail[i].sso_tuyetdoi = formatCurrency(detail[i].sso_tuyetdoi);  
            }                      
        }else if (kyDb == "QUY") {
            detail[i].uoc = formatCurrency(detail[i].uoc);
            detail[i].tong = formatCurrency(detail[i].tong);
            detail[i].thang1 = formatCurrency(detail[i].thang1);
            detail[i].thang2 = formatCurrency(detail[i].thang2);
            detail[i].thang3 = formatCurrency(detail[i].thang3);
            if(detail[i].dbquy!=null){
                detail[i].dbquy = formatCurrency(detail[i].dbquy);
            }
            if(detail[i].sso_phantram!=null){
                detail[i].sso_phantram = formatCurrency(detail[i].sso_phantram);
            }
            if(detail[i].sso_tuyetdoi!=null){
                detail[i].sso_tuyetdoi = formatCurrency(detail[i].sso_tuyetdoi);  
            }
            if(detail[i].chenh_lech!=null){
                detail[i].chenh_lech = formatCurrency(detail[i].chenh_lech);  
            }  
        }else if (kyDb == "THANG" || kyDb == "TUAN") {
           for (var j = 0;j < detail[i].listDetailThang.length;j++) {
                detail[i].listDetailThang[j].gia_tri = formatCurrency(detail[i].listDetailThang[j].gia_tri);
            }
        }
    }

    return detail;
}
function dataTree(data) {
    var arrays = [];
    for (var i = 0;i < data.length;i++) {
        var chucNang = {
            id : data[i].id, text : data[i].ten_Cnang, ma : data[i].ma_Cnang, idcha : data[i].cnang_Cha, children : []
        }
        arrays.push(chucNang);
    }

    return convert(arrays);
}

function convert(arrays) {
    var map = {
    };
    for (var i = 0;i < arrays.length;i++) {
        var obj = arrays[i];
        obj.children = [];

        map[obj.id] = obj;

        var parent = obj.idcha || '-';
        if (!map[parent]) {
            map[parent] = {
                children : []
            };
        }
        map[parent].children.push(obj);
    }

    return map['-'].children;

}

function getDateOfMonth(month, year) {
//    alert(month+": "+ year);
    var d = new Date(year, month, 0);
    var n = d.getDate();

    return n
}

function getNumberDateToDate(toDate, fromDate) {
    var cnt = 0;
    var loop = null;
    loop = new Date(toDate);
    while (loop <= fromDate) {
        var newDate = loop.setDate(loop.getDate() + 1);
        loop = new Date(newDate);
        cnt++;
    }

    return cnt;
}
function setDataFormatMoneyNew(dt, kyDb, dateOfMonth) {
    for (var i = 0;i < dt.length;i++) {
        if (kyDb != "TUAN") {
            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
            dt[i].tong = formatNumberFromStr(dt[i].tong);
        }
        if (kyDb == "NAM") {
            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].tong1 = formatNumberFromStr(dt[i].tong1);
            dt[i].tong2 = formatNumberFromStr(dt[i].tong2);
            dt[i].tong3 = formatNumberFromStr(dt[i].tong3);
            dt[i].tong4 = formatNumberFromStr(dt[i].tong4);
            dt[i].quy1 = formatNumberFromStr(dt[i].quy1);
            dt[i].quy2 = formatNumberFromStr(dt[i].quy2);
            dt[i].quy3 = formatNumberFromStr(dt[i].quy3);
            dt[i].quy4 = formatNumberFromStr(dt[i].quy4);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
            dt[i].thang4 = formatNumberFromStr(dt[i].thang4);
            dt[i].thang5 = formatNumberFromStr(dt[i].thang5);
            dt[i].thang6 = formatNumberFromStr(dt[i].thang6);
            dt[i].thang7 = formatNumberFromStr(dt[i].thang7);
            dt[i].thang8 = formatNumberFromStr(dt[i].thang8);
            dt[i].thang9 = formatNumberFromStr(dt[i].thang9);
            dt[i].thang10 = formatNumberFromStr(dt[i].thang10);
            dt[i].thang11 = formatNumberFromStr(dt[i].thang11);
            dt[i].thang12 = formatNumberFromStr(dt[i].thang12);
        }
        else if (kyDb == "QUY") {
//            dt[i].uoc = formatNumberFromStr(dt[i].uoc);
//            dt[i].tong = formatNumberFromStr(dt[i].tong);
            dt[i].thang1 = formatNumberFromStr(dt[i].thang1);
            dt[i].thang2 = formatNumberFromStr(dt[i].thang2);
            dt[i].thang3 = formatNumberFromStr(dt[i].thang3);
        }
        else if (kyDb == "THANG") {
            for (var j = 1;j <= dateOfMonth;j++) {
                dt[i]['ngay' + j] = formatNumberFromStr(dt[i]['ngay' + j]);
            }
        }
        else if (kyDb == "TUAN") {
            dt[i].gia_tri = formatNumberFromStr(dt[i].gia_tri);
        }
    }
    return dt;
}
function formatNumberFromStrNew(yourNumber,numberRound) {

    if(yourNumber==null || yourNumber == '') return 0;
    var components = yourNumber.toString().replace(/\./g, ',').split(",");
    if(components[0]==""||components[0]==" "){
        components[0]=0;
    }
    if(components[0]=="-"){
    components[0]='-0';
    }
    if (components.length === 1)
        components[0] = yourNumber;
    components[0] = components[0].toString().replace(/(?!-)[^0-9.]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    if (components.length === 2)
        components[1] = components[1].toString().replace(/(?!-)[^0-9.]/g, "").substr(0, numberRound);
    var numStr = components.join(",").substr(0,1)=='-'?'(' +components.join(",").substr(1) + ')':components.join(",");
    return numStr;
    
}
function getArrDateToDate(toDate, fromDate) {
    var date = [];
    for (var d = toDate;d <= fromDate;d.setDate(d.getDate() + 1)) {
        date.push(new Date(d));
    }
    return date;
}

function totalQuayDoc(dt, templates) {
    for (var i = 0;i < templates.length;i++) {
        var total = 0;
        for (var j = 0;j < dt.length;j++) {
            if (dt[j].stt != 'UOC' && dt[j].stt != 'TONG') {
                //total = total+formatNumber(dt[j][templates[i].ten_ctieu]);
                total = new BigNumber(total).add(formatCurrency(dt[j][templates[i].ten_ctieu])).round();
            }
            else if (dt[j].stt == 'TONG') {
                var intPart = total.intPart();
                var decPart = new Number(total.subtract(intPart));
                decPart = eval(decPart).toFixed(2);
                var totalStr = new BigNumber(intPart).add(decPart).toString().replace('\.', ',');
                dt[j][templates[i].ten_ctieu] = replaceCommas(totalStr);
            }
        }
    }
    return dt;
}

function totalQuayDocSGD(dt, templates) {
    for (var i = 0;i < templates.length;i++) {
        var total = 0;
//        var totalUSD = 0;
        for (var j = 0;j < dt.length;j++) {
            if (dt[j].chi_muc != 'Ước' && dt[j].chi_muc != 'Tổng') {
                //total = total+formatNumber(dt[j][templates[i].ten_ctieu]);
                if(templates[i].loai_tien=='TH'){
                  if(templates[i].ma_ctieu!='CHI_DTU'){
                    var month = getMonth(dt[j].chi_muc,"dd/mm/yyyy","/");
                    dt[j][templates[i].ma_ctieu] = calSumBigInt(formatCurrency(dt[j][templates[i].ma_ctieu+'_VND']),tyGiaThang(dt[j][templates[i].ma_ctieu+'_USD'],'USD',month));
                  }
                }
                total = new BigNumber(total).add(formatCurrency(dt[j][templates[i].ma_ctieu])).round();
//                totalUSD = new BigNumber(totalUSD).add(formatCurrency(dt[j][templates[i].ten_ctieu + "USD"])).round();
            } else if (dt[j].chi_muc == 'Tổng') {
                var intPart = total.intPart();
//                var intPartUSD = totalUSD.intPart();
                var decPart = new Number(total.subtract(intPart));
//                var decPartUSD = new Number(totalUSD.subtract(intPartUSD));
//                console.log('VND:'+decPartVND);
//                console.log('USD:'+decPartUSD);
                var totalStr = new BigNumber(intPart).add(decPart).toString().replace('\.', ',');
//                var totalStrUSD = new BigNumber(intPartUSD).add(decPartUSD).toString().replace('\.', ',');
                
                dt[j][templates[i].ma_ctieu] = replaceCommas(totalStr);
//                dt[j][templates[i].ten_ctieu + "USD"] = replaceCommas(totalStrUSD);
            }
        }
    }
    return dt;
}

function tyGiaThang(gia_tri,loai_tien,thang_dbao){
  var tygia = 20000;
  var value = new BigNumber(formatNumberFromStr(gia_tri)).multiply(tygia).toString().replace('\.', ',');
  
  return replaceCommas(value);
}

function changeNumberQuayDoc(dt, templates) {
    for (var i = 0;i < templates.length;i++) {
        var total = 0;
        for (var j = 0;j < dt.length;j++) {
            dt[j][templates[i].ten_ctieu] = formatCurrency(dt[j][templates[i].ten_ctieu]);
        }
    }

    return dt;
}

function calSumBigInt() {
    var argv = calSumBigInt.arguments;
    var argc = argv.length;
    var itemName;
    var itemObj;
    var itemValue;
    var total = 0;
    var totalObj;
    var i;
    var sign;
    for (i = 0;i < argc;i++) {
        itemValue = (argv[i]!=null&&argv[i]!='')?argv[i].toString():"0";
        sign = itemValue.toString().substr(0, 1);
        if (sign == '-' || sign == '+') {
            if(itemValue.toString().length==1){
            itemValue ='0';
            }else{
            itemValue = itemValue.substring(1);
            }
        }
        itemValue = itemValue.replace(/\./g, '');
        itemValue = itemValue.replace(',', '.');
        itemValue = itemValue.replace('(', '-');
        itemValue = itemValue.replace(')', '');
        if (sign == '-') {
            total = new BigNumber(total).minus(itemValue);
        }
        else {
            total = new BigNumber(total).plus(itemValue);
        }
    }    
    return formatNumberFromStr(new BigNumber(total).toString());
}
function calMutiBigIntNew(a,b){     
    a= (a!=null&&a!='')?a.toString().replace(/\./g, '').replace(',', '.'):"1";
    a = a.replace('(', '-');
    a = a.replace(')', '');
    b= (a!=null&&b!='')?b.toString().replace(/\./g, '').replace(',', '.'):"1";
    b = b.replace('(', '-');
    b = b.replace(')', '');
    return formatNumberFromStr(new BigNumber(a).multipliedBy(b).toString());
}
function calMutiBigInt(a,b){     
    a= (a!=null&&a!='')?a.toString().replace(/\./g, '').replace(',', '.'):"1";
    b= (a!=null&&b!='')?b.toString().replace(/\./g, '').replace(',', '.'):"1";
    return formatNumberFromStr(new BigNumber(a).multipliedBy(b).toString());
}
function calMutiBigInt2(a,b){     
    a= (a!=null&&a!='')?a.toString().replace(/\./g, '').replace(',', '.'):"0";
    b= (a!=null&&b!='')?b.toString().replace(/\./g, '').replace(',', '.'):"0";
    return formatNumberFromStr(new BigNumber(a).multipliedBy(b).toString());
}
function calDivBigInt(a,b) {  
    a= (a!=null&&a!='')?a.toString().replace(/\./g, '').replace(',', '.'):"0";
    b= (b!=null&&b!='')?b.toString().replace(/\./g, '').replace(',', '.'):"1";
    return formatNumberFromStr(new BigNumber(a).dividedBy(b).multipliedBy(100).toString());
} 
function calDivBigInt2(a,b) {  
    a= (a!=null&&a!='')?a.toString().replace(/\./g, '').replace(',', '.'):"0";
    b= (b!=null&&b!='')?b.toString().replace(/\./g, '').replace(',', '.'):"1";
    return formatNumberFromStr(new BigNumber(a).dividedBy(b).toString());
} 
function calDivBigIntRS(a,b) { 
    a= (a!=null&&a!=''&& a!=0)?a.toString().replace(/\./g, '').replace(',', '.'):"0";
    b= (b!=null&&b!=''&& b!=0)?b.toString().replace(/\./g, '').replace(',', '.'):"0"; 
    return formatNumberFromStr(new BigNumber(a).minus(b).dividedBy(b).multipliedBy(100).toString()); 
}
function calDivBigIntBD(a,b) {
    return formatNumberFromStr(new BigNumber(a).minus(b).dividedBy(a).multipliedBy(100).toString()); 
}
function calSumBigIntWithTotal() {
    var argv = calSumBigIntWithTotal.arguments;
    var argc = argv.length;
    var itemName;
    var itemObj;
    var itemValue;
    var total = 0;
    var totalObj;
    var i;
    var sign;
    for (i = 1;i < argc;i++) {
        itemValue = argv[i].toString();
        sign = itemValue.toString().substr(0, 1);
        if (sign == '-' || sign == '+') {
            itemValue = itemValue.substring(1);
        }
        itemValue = itemValue.replace(/\./g, '');
        itemValue = itemValue.replace(',', '.')
        if (sign == '-') {
            total = new BigNumber(total).subtract(itemValue).round();
        }
        else {
            total = new BigNumber(total).add(itemValue).round();
        }
    }
    intPart = total.intPart();
    decPart = new Number(total.subtract(intPart));
    decPart = eval(decPart).toFixed(2);
    var totalStr = new BigNumber(intPart).add(decPart).toString().replace('\.', ',');
    argv[0] = replaceCommas(totalStr);
}

function toNumber(numStr) {
    if (numStr == null || numStr.toString()=='') {
        return 0;
    }
    else 
    return new BigNumber(numStr);

}
function toNumberStr(numStr) {
    if (numStr == null || numStr.toString() =='') {
        return 0;
    }
    else 
    return new BigNumber(numStr);

}

function getFullYear() {
    var d = new Date();

    return d.getFullYear();
}

function randomNum(value,num){
  return 1234+value+3247*num;
}

function getMonth(_date, _format, _delimiter){
  var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    
    return (formatedDate.getMonth()+ 1);
}

function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    return formatedDate;
}

function uocQuy(quy, nam) {
    var result = "";
    if (quy == "I") {
        result = "IV/" + parseInt(nam - 1);
    }
    else if (quy == "II") {
        result = "I/" + nam;
    }
    else if (quy == "III") {
        result = "II/" + nam;
    }
    else if (quy == "IV") {
        result = "III/" + nam;
    }

    return result;
}

function thangQuy(quy, thang) {
    var result = 1;
    if (quy == "I") {
        result = thang;
    }
    else if (quy == "II") {
        result = (2 * 3) - 3 + thang;
    }
    else if (quy == "III") {
        result = (3 * 3) - 3 + thang;
    }
    else if (quy == "IV") {
        result = (4 * 3) - 3 + thang;
    }

    return result;
}

function thangQuyDB(quyDBao, thang) {
    var result = 1;
    if (quyDBao == "1") {
        result = thang;
    }
    else if (quyDBao == "2") {
        result = (2 * 3) - 3 + thang;
    }
    else if (quyDBao == "3") {
        result = (3 * 3) - 3 + thang;
    }
    else if (quyDBao == "4") {
        result = (4 * 3) - 3 + thang;
    }

    return result;
}
function thangtoThangThu(quyDbao,thang){
    var result=0;
    if(quyDbao=='I'||quyDbao=='1'){
      result = thang;
    }else if(quyDbao=='II'||quyDbao=='2'){
     result =  thang - 3;
    }else if(quyDbao=='III'||quyDbao=='3'){
     result = thang - 6;
    }else{
     result =thang - 9;
    }
    return result;
}
function uocThang(thang, nam) {
    var result = "";
    if (thang == "1") {
        result = "12/" + (parseInt(nam) - 1);
    } else {
        result = (parseInt(thang)-1) +"/" + nam;
    }

    return result;
}
function getDateStr(date) {
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;  
  return day + '/' + month + '/' + year;
}
//function checkDLnhap(objDB){
//        for(var i=0; i < objDB.listDBaoCTiet.length;i++){
//            var obj = objDB.listDBaoCTiet[i];
//            if(obj.is_tong==1){
//            var uoc=0;
//            var quy1=0;
//            var quy2=0;
//            var quy3=0;
//            var quy4=0;
//            if(objDB.ky_dbao=='NAM'){
//                for(var h=0; h < objDB.listDBaoCTiet.length;h++){
//                    if(objDB.listDBaoCTiet[h].chi_tieu_cha == obj.ma_ctieu){
//                        if(objDB.listDBaoCTiet[h].loai_tien=='VND'){
//                            quy1 = calSumBigInt(quy1, calMutiBigInt2(objDB.listDBaoCTiet[h].quy1,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].quy2,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].quy3,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            quy4 = calSumBigInt(quy4,calMutiBigInt2(objDB.listDBaoCTiet[h].quy4,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            uoc =  calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                        }else{
//                            quy1 = calSumBigInt(quy1, calMutiBigInt2(objDB.listDBaoCTiet[h].quy1,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].quy2, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].quy3, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            quy4 = calSumBigInt(quy4,calMutiBigInt2(objDB.listDBaoCTiet[h].quy4, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,    calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                        }
//                    }      
//                }
//               if(Number(formatCurrency(obj.quy1)) < Number(formatCurrency(quy1)) || 
//                  Number(formatCurrency(obj.quy2)) < Number(formatCurrency(quy2))||
//                  Number(formatCurrency(obj.quy3)) < Number(formatCurrency(quy3))||
//                  Number(formatCurrency(obj.quy4)) < Number(formatCurrency(quy4))||
//                  Number(formatCurrency(obj.uoc)) < Number(formatCurrency(uoc))){
//               
//                return false;
//                }
//            }else if(objDB.ky_dbao=='QUY'){ 
//               for(var h=0; h < objDB.listDBaoCTiet.length;h++){
//                    if( objDB.listDBaoCTiet[h].chi_tieu_cha == obj.ma_ctieu){
//                       if(objDB.listDBaoCTiet[h].loai_tien=='VND'){
//                            quy1 = calSumBigInt(quy1,calMutiBigInt2(objDB.listDBaoCTiet[h].thang1,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].thang2,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].thang3,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
//                       }else{
//                            quy1 = calSumBigInt(quy1,calMutiBigInt2(objDB.listDBaoCTiet[h].thang1,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].thang2,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].thang3,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,     calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
//                       
//                       }
//                    }      
//                }
//                if(Number(formatCurrency(obj.thang1)) < Number(formatCurrency(quy1))||
//                    Number(formatCurrency(obj.thang2)) < Number(formatCurrency(quy2))||
//                    Number(formatCurrency(obj.thang3)) < Number(formatCurrency(quy3))||
//                    Number(formatCurrency(obj.uoc)) < Number(formatCurrency(uoc))){
//                
//                return false;
//                }
//            }
//            }
//        }
//        return true;
//    }
function checkDLnhap(objDB){
        for(var i=0; i < objDB.listDBaoCTiet.length;i++){
            var obj = objDB.listDBaoCTiet[i];
            if(obj.is_tong==1){
            var uoc=0;
            var quy1=0;
            var quy2=0;
            var quy3=0;
            var quy4=0;
            if(objDB.ky_dbao=='NAM'){
                for(var h=0; h < objDB.listDBaoCTiet.length;h++){
                    if(objDB.listDBaoCTiet[h].chi_tieu_cha == obj.ma_ctieu){
                        if(objDB.listDBaoCTiet[h].loai_tien=='VND'){
                            quy1 = calSumBigInt(quy1, calMutiBigInt2(objDB.listDBaoCTiet[h].quy1,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].quy2,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].quy3,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            quy4 = calSumBigInt(quy4,calMutiBigInt2(objDB.listDBaoCTiet[h].quy4,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            uoc =  calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                        }else{
                            quy1 = calSumBigInt(quy1, calMutiBigInt2(objDB.listDBaoCTiet[h].quy1,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].quy2, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].quy3, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            quy4 = calSumBigInt(quy4,calMutiBigInt2(objDB.listDBaoCTiet[h].quy4, calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,    calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                        }
                    }      
                }
               if(Number(formatCurrency(obj.quy1)) < Number(formatCurrency(quy1))){
                  $("#quy1_"+i).focus();
                  return false;
                }
               if(Number(formatCurrency(obj.quy2)) < Number(formatCurrency(quy2))){
                  $("#quy2_"+i).focus();
                  return false;
                }
                if(Number(formatCurrency(obj.quy3)) < Number(formatCurrency(quy3))){
                  $("#quy3_"+i).focus();
                  return false;
                }
                if(Number(formatCurrency(obj.quy4)) < Number(formatCurrency(quy4))){
                  $("#quy4_"+i).focus();
                  return false;
                }
              if(Number(formatCurrency(obj.uoc)) < Number(formatCurrency(uoc))){
                  $("#uoc_"+i).focus();
                  return false;
              }
                  
            }else if(objDB.ky_dbao=='QUY'){ 
               for(var h=0; h < objDB.listDBaoCTiet.length;h++){
                    if( objDB.listDBaoCTiet[h].chi_tieu_cha == obj.ma_ctieu){
                       if(objDB.listDBaoCTiet[h].loai_tien=='VND'){
                            quy1 = calSumBigInt(quy1,calMutiBigInt2(objDB.listDBaoCTiet[h].thang1,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].thang2,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].thang3,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia)));
                       }else{
                            quy1 = calSumBigInt(quy1,calMutiBigInt2(objDB.listDBaoCTiet[h].thang1,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            quy2 = calSumBigInt(quy2,calMutiBigInt2(objDB.listDBaoCTiet[h].thang2,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            quy3 = calSumBigInt(quy3,calMutiBigInt2(objDB.listDBaoCTiet[h].thang3,calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                            uoc = calSumBigInt(uoc,calMutiBigInt2(objDB.listDBaoCTiet[h].uoc,     calDivBigInt2( formatNumberFromStr(objDB.listDBaoCTiet[h].ty_gia),"1.000")));
                       
                       }
                    }      
                }
//                if( Number(formatCurrency(obj.thang1)) < Number(formatCurrency(quy1))||
//                    Number(formatCurrency(obj.thang2)) < Number(formatCurrency(quy2))||
//                    Number(formatCurrency(obj.thang3)) < Number(formatCurrency(quy3))||
//                    Number(formatCurrency(obj.uoc))    < Number(formatCurrency(uoc))){
//                
//                      return false;
//                }
              if(Number(formatCurrency(obj.thang1)) < Number(formatCurrency(quy1))){
                  $("#thang1_"+i).focus();
                  return false;
                }
               if(Number(formatCurrency(obj.thang2)) < Number(formatCurrency(quy2))){
                  $("#thang2_"+i).focus();
                  return false;
                }
                if(Number(formatCurrency(obj.thang3)) < Number(formatCurrency(quy3))){
                  $("#thang3_"+i).focus();
                  return false;
                }
              if(Number(formatCurrency(obj.uoc)) < Number(formatCurrency(uoc))){
                  $("#uoc_"+i).focus();
                  return false;
              }
                
            }
            }
        }
        return true;
    }
    scollHeader = function(){
               $('.khung_scoll').each(function(){             
               //hoa sy truong code
               $('.fix-tablet-thead, .scroll-gia').remove();
                  var table = $(this).find('table');
                  var tableW = table.removeAttr('style').css('width', table.outerWidth());
                  table.find('thead th').each(function(){
                    $(this).removeAttr('style').css('width', $(this).outerWidth());
                  });
                  var thead = table.find('thead').clone();
                  var newTable = '<div class="scroll-gia"><div class="scroll-bar-gia" style="height: '+table.outerHeight()+'px"></div></div><div class="fix-tablet-thead"><table class="table table-striped table-bordered" style="width: '+table.outerWidth()+'px"></table></div>';
                  $(newTable).prependTo($(this)).find('table').append(thead);
                $(this).find('.scroll-gia').css('left',$(this).scrollLeft()+$(this).outerWidth()-17);
                $(this).scroll(function(){
                  $(this).find('.scroll-gia').css('left',$(this).scrollLeft()+$(this).outerWidth()-17);
                });
                 $(this).find('.scroll-y').scroll(function(){
                  $(this).parent().find('.scroll-gia').scrollTop($(this).scrollTop());
                });
                var scroll_num = 0;
                $(this).find('.scroll-y').on('mousewheel DOMMouseScroll', function(e){
                  $(this).parent().find('.scroll-gia').scrollTop($(this).scrollTop());
                  scroll_num = 1;
                  clearTimeout($.data(this, 'timer'));
                  $.data(this, 'timer', setTimeout(function() {
                     scroll_num = 0;
                  }, 250));
                });
                $(this).find('.scroll-gia').scroll(function(){
                 if(scroll_num == 0){$(this).parent().find('.scroll-y').scrollTop($(this).scrollTop());}
                });
                });
      }
      
      scollHeaderNew = function(ten_khung){
               $('.' + ten_khung).each(function(){             
               //hoa sy truong code
               $('.fix-tablet-thead, .scroll-gia').remove();
                  var table = $(this).find('table');
                  var tableW = table.removeAttr('style').css('width', table.outerWidth());
                  table.find('thead th').each(function(){
                    if($(this).attr("class") == 'min-with-col'){
                        console.log($(this).outerWidth());
                        $(this).removeAttr('style').css('min-width', "200px");
                    }else if($(this).attr("class") == 'min-with-code'){
                        console.log($(this).outerWidth());
                        $(this).removeAttr('style').css('min-width', "100px");
                    }else{
                        $(this).removeAttr('style').css('width', $(this).outerWidth());
                    }
                  });
                  var thead = table.find('thead').clone();
                  var newTable = '<div class="scroll-gia"><div class="scroll-bar-gia" style="height: '+table.outerHeight()+'px"></div></div><div class="fix-tablet-thead"><table class="table table-striped table-bordered" style="width: '+table.outerWidth()+'px"></table></div>';
                  $(newTable).prependTo($(this)).find('table').append(thead);
                $(this).find('.scroll-gia').css('left',$(this).scrollLeft()+$(this).outerWidth()-17);
                $(this).scroll(function(){
                  $(this).find('.scroll-gia').css('left',$(this).scrollLeft()+$(this).outerWidth()-17);
                });
                 $(this).find('.scroll-y').scroll(function(){
                  $(this).parent().find('.scroll-gia').scrollTop($(this).scrollTop());
                });
                var scroll_num = 0;
                $(this).find('.scroll-y').on('mousewheel DOMMouseScroll', function(e){
                  $(this).parent().find('.scroll-gia').scrollTop($(this).scrollTop());
                  scroll_num = 1;
                  clearTimeout($.data(this, 'timer'));
                  $.data(this, 'timer', setTimeout(function() {
                     scroll_num = 0;
                  }, 250));
                });
                $(this).find('.scroll-gia').scroll(function(){
                 if(scroll_num == 0){$(this).parent().find('.scroll-y').scrollTop($(this).scrollTop());}
                });
                });
      }
      
      function checkTrangThai(values){ 
        var trang_thai = returnValueByField(values,'TRANG_THAI');
        if(trang_thai!='03'&&trang_thai!='05'
           &&trang_thai!='06'&&trang_thai!='04'
           &&trang_thai!='07'){
            return true;
        }
         
      } 
      
      function returnValueByField(values,field){
        try{
          var arrLichSu = values.split(';');
          for(var j=0;j<arrLichSu.length;j++){
              if(arrLichSu[j].split('=')[0]==field){
                  return arrLichSu[j].split('=')[1];
              }
          }
        }catch(err){
        }
      } 
      
     function checkTrangThaiNhap(values){ 
        var trang_thai = returnValueByFieldNhap(values,'TRANG_THAI');
        var ts_duyet = returnValueByFieldNhap(values,'TS_DUYET');
        if(trang_thai!='02'&&trang_thai!='05'&&trang_thai!='04'
            ){
            if(trang_thai=='03'&&ts_duyet=='1'){
              return false;
            }else{
              return true;
            }
        }
      } 
      
      function checkTrangThaiNhapThang(values){ 
        var trang_thai = returnValueByFieldNhap(values,'TRANG_THAI');
        var ts_duyet = returnValueByFieldNhap(values,'TS_DUYET');
        if(trang_thai!='02'&&trang_thai!='05'&&trang_thai!='04'
            ){
            if(trang_thai=='03'&&ts_duyet=='1'){
              return false;
            }else{
              return true;
            }
        }
      } 
      
       function returnValueByFieldNhap(values,field){
        var arrLichSunhap = values.split(';');
        for(var j=0;j<arrLichSunhap.length;j++){
            var strArr = arrLichSunhap[j].split('=');
            if(strArr[0]==field){
                strValue ="";
                if(strArr.length>2){
                  strValue = strArr[1];
                  for(var i=2;i<strArr.length;i++){
                    strValue +='='+strArr[i];
                  }
                }else if(strArr.length==2){
                  strValue = strArr[1];
                }
                return strValue;
            }
        }
      }
      
      function checkTrangThai1(values){ 
        var trang_thai = returnValue(values,'TRANG_THAI');
        if(trang_thai!='03'&&trang_thai!='05'
           &&trang_thai!='06'&&trang_thai!='04'
           &&trang_thai!='07'){
            return true;
        }
         
      } 
       
      
      function returnValue(values,field){ 
        try{
          var arrLichSu = values.split(';');
          for(var j=0;j<arrLichSu.length;j++){
              if(arrLichSu[j].split('=')[0]==field){
                  return arrLichSu[j].split('=')[1];
              }
          }
        }catch(err){
        }
      }
       
      function returnValueByFieldTest(values,TRANG_THAI){
        try{ 
          var arrLichSu = old_values.split(';');
          for(var j=0;j<arrLichSu.length;j++){
               
          }
        }catch(err){
        }
      } 
      
     