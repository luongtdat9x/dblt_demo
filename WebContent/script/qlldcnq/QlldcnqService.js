'use strict';
app.service('QlldcnqService', ['BaseServices', function (BaseServices) {
var urls = {
        getAll : "qlldcnqagl?action=list",
        insertQlldcnq : "qlldcnqagl?action=add",
        updateQlldcnq : "qlldcnqagl?action=update", 
        deleteQlldcnq : "qlldcnqagl?action=delete",
        viewlog : "lognqagl?action=viewlog",
        viewlogdetail : "lognqagl?action=viewlogdetail"
    }
function getAll(data) {
      return BaseServices.callAPI(urls.getAll, 'POST', data);
    }
function insertQlldcnq(data) {
      return BaseServices.callAPI(urls.insertQlldcnq, 'POST', data);
    }
function updateQlldcnq(data) {
      return BaseServices.callAPI(urls.updateQlldcnq, 'POST', data);
    }
function deleteQlldcnq(data) {
      return BaseServices.callAPI(urls.deleteQlldcnq, 'POST', data);
    }
function viewlog(data) {
      return BaseServices.callAPI(urls.viewlog, 'POST', data);
    }
function viewlogdetail(data) {
      return BaseServices.callAPI(urls.viewlogdetail, 'POST', data);
    }
var service = {
        getAll : getAll,
        insertQlldcnq : insertQlldcnq,
        updateQlldcnq : updateQlldcnq,
        deleteQlldcnq : deleteQlldcnq,
        viewlog : viewlog,
        viewlogdetail : viewlogdetail
    };return service;
}
]);