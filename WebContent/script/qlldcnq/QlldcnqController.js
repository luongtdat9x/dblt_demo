app.controller('QlldcnqController',['$scope','QlldcnqService', function ($scope,QlldcnqService) {
	//phan trang 
  $scope.totalItems = 0;
  $scope.currentPage = 1;
  $scope.maxSize = 10; //Number of pager buttons to show
  
  //khai bao
  $scope.displayList = 'block';
  $scope.displayAdd = 'none';
  $scope.displayEdit = 'none';
  $scope.displayView = 'none';
  $scope.displayLog = 'none';
  $scope.search = {};
  $scope.dbLenh = {};
  $scope.dbLenhOld = {};
  $scope.dbLenhNew = {};
  $scope.listLenhs = [];
  $scope.listNams = [];
  $scope.listNHTMs = [];
  $scope.phuLucs = [];
  $scope.idAuto = "";
  $scope.listAlerts = [];
  $scope.detailAlerts = [];
  $scope.donViKBNN = "";
  $scope.diaChiKBNN = "";
  $scope.soTienBangChu = "";
  $scope.tenTKN = "";
  $scope.noiMoTKN = "";
  $scope.emptyFromDate = false;
  $scope.emptyToDate = false;
  $scope.errorDate = false;
  
  var trangThais = [];
  trangThais.push( {
      key : "01", value : "Dự thảo"
  });  
//  trangThais.push( {
//      key : "02", value : "Đã điều chỉnh"
//  }); 
  trangThais.push( {
      key : "03", value : "Chờ LĐ Phòng duyệt"
  });  
  trangThais.push( {
      key : "04", value : "Từ chối"
  }); 
//  trangThais.push( {
//      key : "05", value : "LĐ phòng đã duyệt"
//  });  
  trangThais.push( {
      key : "06", value : "Chờ LĐ Cục duyệt"
  }); 
  trangThais.push( {
      key : "07", value : "LĐ Cục đã duyệt"
  });  
  trangThais.push( {
      key : "08", value : "Đã hủy"
  }); 
  $scope.trangThais = trangThais;
  
  var tinhTrangBCs = [];
  tinhTrangBCs.push( {
      key : "01", value : "Chờ LĐ KBNN duyệt"
  });  
  tinhTrangBCs.push( {
      key : "02", value : "LĐ KBNN đã duyệt"
  }); 
  $scope.tinhTrangBCs = tinhTrangBCs;
  
        $scope.collManipulation = [];
        $scope.collManipulation.push({
            key: "I",
            value: "Thêm mới"
        });
        $scope.collManipulation.push({
            key: "U",
            value: "Sửa"
        });
        $scope.collManipulation.push({
            key: "D",
            value: "Xóa"
        });

   init();
function init() {
   $scope.displayList = 'block';
  $scope.displayAdd = 'none';
  $scope.displayEdit = 'none';
  $scope.displayView = 'none';
  $scope.displayLog = 'none';
   $scope.search.rowsInPage = $scope.maxSize;
   $scope.search.pageIndex = $scope.currentPage;
   $scope.listAlerts = [];
  $scope.detailAlerts = [];
   
   jQuery('#content_body').showLoading();
   return QlldcnqService.getAll(({"search":$scope.search})).then(function (data) {   
            if(data.msg.errCode == "0"){
              $scope.dbLenh = {};
              $scope.noiDungThanhToan = "";
              $scope.donViKBNN = "";
              $scope.diaChiKBNN = "";
              $scope.soTienBangChu = "";
              $scope.tenTKN = "";
              $scope.noiMoTKN = "";
              $scope.dbLenhOld = {};
                
              $scope.listLenhs = data.msg.object.listLenhs;
              $scope.listNams = data.msg.object.listNams;
              $scope.listNHTMs = data.msg.object.listNHTMs;
              $scope.listTKKBNNs = data.msg.object.listTKKBNNs;
              $scope.phuLucs = data.msg.object.phuLucs;
              $scope.totalItems = data.msg.object.totalItems;
              $scope.idAuto = data.msg.object.idAuto;
            }else{
              $scope.listAlerts.push({ type: 'error', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
            }
            jQuery('#content_body').hideLoading();
        },function (error) {
            checkError(error);
        });
    }
    $scope.viewAdd = function () {
        $scope.dbLenh = {};
        var dateNow = moment().format('YYYY MM DD');
        $scope.dbLenh.ngayTao = dateNow.substr(8,2)+"/"+dateNow.substr(5,2)+"/"+dateNow.substr(0,4);
        $scope.dbLenh.soLenh = $scope.idAuto + "" + dateNow.substr(0,4) + "/L-KBNN";
        $scope.displayList = 'none';
        $scope.displayAdd = 'block';
        $scope.displayEdit = 'none';
        $scope.displayView = 'none';
    };
    $scope.edit = function (index) {
        $scope.checkButton = false;
        $scope.dbLenh = $scope.listLenhs[index];
        $scope.selectKBNN();
        $scope.selectPL();
        var strSo = docso(parseInt($scope.dbLenh.soTien.replace(/[^0-9\s]/gi, ''))) + " tỷ đồng";
        $scope.soTienBangChu = strSo.substr(1,1).toUpperCase() + strSo.substr(2);
        $scope.dbLenh.soTien = formatNumberFromStr($scope.dbLenh.soTien);
        if($scope.dbLenh.trangThai == '04') {
            $scope.dbLenh.ndtt = "";
            $scope.dbLenh.ldTC = "";
        }
        $scope.displayList = 'none';
        $scope.displayAdd = 'none';
        $scope.displayEdit = 'block';
        $scope.displayView = 'none';
        $scope.displayLog = 'block';
        $scope.displayDetailLog = 'none';
    };
    $scope.view = function (index) {
        $scope.dbLenh = $scope.listLenhs[index];
        $scope.selectKBNN();
        $scope.selectPL();
        var strSo = docso(parseInt($scope.dbLenh.soTien.replace(/[^0-9\s]/gi, ''))) + " tỷ đồng";
        $scope.soTienBangChu = strSo.substr(1,1).toUpperCase() + strSo.substr(2);
        $scope.dbLenh.soTien = formatNumberFromStr($scope.dbLenh.soTien); 
        $scope.displayList = 'none';
        $scope.displayAdd = 'none';
        $scope.displayEdit = 'none';
        $scope.displayView = 'block';
        $scope.displayLog = 'block';
        $scope.displayDetailLog = 'none';
    };
    $scope.back = function () { 
        if (!$scope.changedate()) {
          $scope.search.ngayLenh1 = "";
          $scope.search.ngayLenh2 = "";
          $scope.emptyFromDate = false;
          $scope.emptyToDate = false;
          $scope.errorDate = false;
        }
        init();
    };
    $scope.selectKBNN = function () { 
        if($scope.dbLenh.idTKC == "") {
            $scope.donViKBNN = "";
            $scope.diaChiKBNN = "";
        }
        else {
          for(var i = 0; i < $scope.listTKKBNNs.length; i++) {
            if($scope.listTKKBNNs[i].id == $scope.dbLenh.idTKC) {
              $scope.donViKBNN = $scope.listTKKBNNs[i].tenDonVi;
              $scope.diaChiKBNN = $scope.listTKKBNNs[i].diaChi;
            }
          }
        }
    };
    $scope.selectPL = function () { 
        if($scope.dbLenh.idPL == "") {
          $scope.dbLenh.soTien = "";
          $scope.soTienBangChu = "";
          $scope.dbLenh.ndtt = "";
          $scope.tenTKN = "";
          $scope.noiMoTKN = "";
        }
        else {
          for(var i = 0; i < $scope.phuLucs.length; i++) {
            if($scope.phuLucs[i].id == $scope.dbLenh.idPL) {
              $scope.dbLenh.idNHTM = $scope.phuLucs[i].idNHTM;
              $scope.tenTKN = $scope.phuLucs[i].tenTKN;
              $scope.noiMoTKN = $scope.phuLucs[i].noiMoTKN;
              $scope.dbLenh.soTien = formatNumberFromStr($scope.phuLucs[i].soTien);
              var strSo = docso(parseInt($scope.dbLenh.soTien.replace(/[^0-9\s]/gi, ''))) + " tỷ đồng";
              $scope.soTienBangChu = strSo.substr(1,1).toUpperCase() + strSo.substr(2);
            }
          }
        }
    };
    $scope.save = function (typeAdd) {
        $scope.listAlerts = [];
        $scope.detailAlerts = [];
        
        if($scope.dbLenh.idTKC == null || $scope.dbLenh.idTKC == "") {
            $scope.detailAlerts.push({ type: 'danger',errMessage: "Vui lòng chọn tài khoản chuyển!" });
            $("#txtTKC").focus();
            return;
        }
        if($scope.dbLenh.idPL == null || $scope.dbLenh.idPL == "") {
            $scope.detailAlerts.push({ type: 'danger',errMessage: "Vui lòng chọn phụ lục hợp đồng!" });
            $("#txtPL").focus();
            return;
        }
        $scope.dbLenh.soTien = $scope.dbLenh.soTien.replace(/[^0-9\s]/gi, '');
        if($scope.dbLenh.ndtt == null || $scope.dbLenh.ndtt == "") {
            $scope.detailAlerts.push({ type: 'danger',errMessage: "Vui lòng nhập nội dung thanh toán!" });
            $("#txtNDTT").focus();
            return;
        }
        
        return QlldcnqService.updateQlldcnq({"typeAdd":typeAdd, "dbLenh":$scope.dbLenh}).then(function (data) {
            if(data.msg.errCode == "0"){
              $scope.search = {};
              init();
              $scope.displayList = 'block';
              $scope.displayEdit = 'none';
              $scope.displayAdd = 'none';
              $scope.displayView = 'none';
              $scope.listAlerts.push({ type: 'success', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
            }else{
              $scope.detailAlerts.push({ type: 'danger', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
            }
        },
        function (error) {
            checkError(error);
        });
    };
    $scope.add = function (typeAdd) {
        $scope.listAlerts = [];
        $scope.detailAlerts = [];
        $scope.dbLenh.id = $scope.idAuto;
        
        if($scope.dbLenh.idTKC == null || $scope.dbLenh.idTKC == "") {
            $scope.detailAlerts.push({ type: 'danger', errMessage: "Vui lòng chọn tài khoản chuyển!" });
            $("#txtTKC").focus();
            return;
        }
        if($scope.dbLenh.idPL == null || $scope.dbLenh.idPL == "") {
            $scope.detailAlerts.push({ type: 'danger', errMessage: "Vui lòng chọn phụ lục hợp đồng!" });
            $("#txtPL").focus();
            return;
        }
        $scope.dbLenh.soTien = $scope.dbLenh.soTien.replace(/[^0-9\s]/gi, '');
        if($scope.dbLenh.ndtt == null || $scope.dbLenh.ndtt == "") {
            $scope.detailAlerts.push({ type: 'danger', errMessage: "Vui lòng nhập nội dung thanh toán!" });
            $("#txtNDTT").focus();
            return;
        }

        return QlldcnqService.insertQlldcnq({"typeAdd":typeAdd, "dbLenh":$scope.dbLenh}).then(function (data) {
            if(data.msg.errCode == "0"){
              init();
              $scope.displayList = 'block';
              $scope.displayEdit = 'none';
              $scope.displayAdd = 'none';
              $scope.displayView = 'none';
              $scope.listAlerts.push({ type: 'success', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
            }else{
              $scope.detailAlerts.push({ type: 'danger', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
            }
        },
        function (error) {
            checkError(error);
        });
    };
    $scope.delt = function (id, index) {
        $scope.dbLenh = $scope.listLenhs[index];
        if($scope.dbLenh.trangThai == '04') {
            if(confirm("Bạn có chắc chắn muốn hủy thông tin này không?")){ 
            return QlldcnqService.deleteQlldcnq({"idSelect":id, "dbLenh":$scope.dbLenh, "typeAdd":"huy"}).then(function (data) {
                    if(data.msg.errCode == "0"){
                      init();
                      $scope.listAlerts=[];
                      $scope.listAlerts.push({ type: 'success', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
                    }else{
                      $scope.listAlerts=[];
                      $scope.listAlerts.push({ type: 'danger', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
                    }
                }, function (error) {
                  checkError(error);
                });
            }   
        }
        if(confirm("Bạn có chắc chắn muốn xóa thông tin này không?")){ 
        return QlldcnqService.deleteQlldcnq({"idSelect":id}).then(function (data) {
                if(data.msg.errCode == "0"){
                  init();
                  $scope.listAlerts=[];
                  $scope.listAlerts.push({ type: 'success', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
                }else{
                  $scope.listAlerts=[];
                  $scope.listAlerts.push({ type: 'danger', errCode: data.msg.errCode,errMessage: data.msg.errMessage });
                }
            }, function (error) {
              checkError(error);
            });
        }   
    };
    $scope.find = function (pageNo) {
        $scope.listAlerts = [];
        $scope.detailAlerts = [];
        if($scope.search.nam != "" && $scope.search.nam != null) {
          $scope.search.nam = parseInt($scope.search.nam) + "";
        }
        if (!$scope.changedate()) {
                if ($scope.emptyFromDate) {
                    $("#txtLenhTu").focus();
                }
                if ($scope.emptyToDate || $scope.errorDate) {
                    $("#txtLenhDen").focus();
                }
                return false;
            }
      init();
   };
    $scope.pageChanged = function () {
    	init();
    };
    $scope.changedate = function() {
            $scope.emptyFromDate = false;
            $scope.emptyToDate = false;
            $scope.errorDate = false;
            $scope.checkEffectiveDate = false;
            if ($scope.search.ngayLenh1 != null && $scope.search.ngayLenh1 != '') {
                if (!moment($scope.search.ngayLenh1, 'DD/MM/YYYY', true).isValid()) {
                    $scope.emptyFromDate = true;
                    return false;
                }
            }
            if ($scope.search.ngayLenh2 != null && $scope.search.ngayLenh2 != '') {
                if (!moment($scope.search.ngayLenh2, 'DD/MM/YYYY', true).isValid()) {
                    $scope.emptyToDate = true;
                    return false;
                }
            }
            if (($scope.search.ngayLenh1 != null && $scope.search.ngayLenh1 != '') &&
                ($scope.search.ngayLenh2 != null && $scope.search.ngayLenh2 != '')) {
                var startDate = parseDate($scope.search.ngayLenh1).getTime();
                var endDate = parseDate($scope.search.ngayLenh2).getTime();
                if (startDate > endDate) {
                    $scope.errorDate = true;
                    return false;
                }
            }
            return true;
        };
    function parseDate(str) {
            var mdy = str.split("/");
            var m = toNumber(mdy[1]) - 1;
            return new Date(mdy[2], m, mdy[0]);
        };
    $scope.thoat = function() {
      window.location.href = "main";
    };
    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;width: 96%;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
    
    
    $scope.log = function() {
            $scope.displayDetailLog = 'block';
            $scope.logFlag = true;
            $scope.detailLogFlag = false;
            $scope.search = {};
            $scope.search.record_id = $scope.dbLenh.id;
            $scope.search.table_name = "NQ_LOG_DIEU_CHUYEN_QUY";
            jQuery('#content_body').showLoading();
            Data = QlldcnqService.viewlog({
                "search": $scope.search
            });
            Data.then(function(data) {
                    if (data.msg.errCode == "0") {
                        $scope.lsLog = data.msg.object;
                        $scope.listAlerts.push({
                            type: 'success',
                            errCode: data.msg.errCode,
                            errMessage: data.msg.errMessage
                        });
                    } else {
                        $scope.detailAlerts.push({
                            type: 'danger',
                            errCode: data.msg.errCode,
                            errMessage: data.msg.errMessage
                        });
                    }
                    jQuery('#content_body').hideLoading();
                },
                function(error) {
                    checkError(error);
                });
        };

        $scope.viewDetailLog = function(index) {
            $scope.logFlag = false;
            $scope.detailLogFlag = true;
            $scope.search = {};
            $scope.search.log_type = $scope.lsLog[index].log_type;
            $scope.dbLenhOld = JSON.parse($scope.lsLog[index].old_values);
            $scope.dbLenhNew = JSON.parse($scope.lsLog[index].new_values);
        }
        $scope.hideDetail = function() {
            $scope.logFlag = true;
            $scope.detailLogFlag = false;
        }
   
}]);