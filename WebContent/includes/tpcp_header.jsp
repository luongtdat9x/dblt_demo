<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="common.AppConstants"%>
<HTML ng-app='MyApp'>
<HEAD>
<TITLE>DBLT-HỆ THỐNG DỰ BÁO LUỒNG TIỀN</TITLE>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<META content="text/html; charset=UTF-8" http-equiv="Content-Type"></META>
<!--<script type="text/javascript" src="styles/js/jquery-1.8.3.js"></script>-->
<LINK rel="stylesheet" type="text/css"
	href="styles/jquery-ui-1.11.3/jquery-ui.css" media="screen" />
<LINK rel="stylesheet" type="text/css" href="styles/css/khobac.css"
	media="screen" />
<LINK rel="stylesheet" type="text/css" href="styles/css/style.css"
	media="screen" />
<LINK rel="stylesheet" type="text/css" href="styles/css/style1.css"
	media="screen" />
<SCRIPT type="text/javascript" src="styles/js/jquery/respond.src.js"></SCRIPT>
<LINK rel="stylesheet" type="text/css" href="styles/css/oracle.css"
	media="screen" />
<link rel="stylesheet" type="text/css"
	href="styles/js/calendar/blue.css" />
<SCRIPT type="text/javascript" src="styles/js/moment.js"></SCRIPT>

<META content="no-cache" http-equiv="Pragma"></META>
<META content="-1" http-equiv="Expires"></META>
<META name="GENERATOR" content="MSHTML 8.00.7600.16821"></META>
<script src="styles/js/jquery/jquery-1.11.3.min.js"></script>
<!--date-->
<script src="angularjs/v1.5.8/angular.js"></script>
<script src="angularjs/v1.5.8/angular-animate.js"></script>
<script src="angularjs/v1.5.8/angular-sanitize.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="styles/js/jquery/bootstrap.js"></script>
<!--<script src="styles/js/jquery/bootstrap-select.js"></script>-->
<script src="styles/jquery-ui-1.11.3/jquery-ui.js"></script>
<script src="script/common/bigFloat.js"></script>
<script src="script/common/common.js"></script>

<!-- angularjs -->

<script type="text/javascript" src="angularjs/angular.min.js"></script>
<script type="text/javascript" src="angularjs/angular-cookies.js"></script>
<script src="angularjs/ngStorage.min.js"></script>
<script src="angularjs/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="angularjs/js/hotkeys.js"></script>
<LINK rel="stylesheet" type="text/css"
	href="styles/css/bootstrap.min.css" media="screen" />
<LINK rel="stylesheet" type="text/css" href="styles/css/showLoading.css"
	media="screen" />
<script src="styles/js/jquery.showLoading.js"></script>
<script src="script/module.js"></script>
<script src="script/BaseServices.js"></script>
<script src="styles/js/jquery/bootstrap-select.js"></script>
<!-- angularjs -->
<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="styles/js/jquery/selectivizr.js"></script>
  <noscript><link rel="stylesheet" href="[fallback css]" /></noscript>
  <![endif]-->
<style type="text/css">
.hover-item {
	background: #777777;
}

.hover-item:hover {
	color: #2196F3;
	background: #fff;
}
</style>
</HEAD>

<BODY id="content_body" onload="closeLoading();">
	<%
		if (request.getSession().getAttribute(AppConstants.APP_USER_ID_SESSION) == null) {
		response.sendRedirect(request.getContextPath() + "/login");
	}
	%>
	<SCRIPT type="text/javascript">
jQuery('#content_body').showLoading();
function closeLoading(){
   $(".disPicker").datepicker( {
          dateFormat : "dd/mm/yy"
      });
    setTimeout(function(){jQuery('#content_body').hideLoading();},500);
}
$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};
//// ------------------------- new
function formatSoAm(obj, evt) {
	var currentStart = obj.value.length - obj.selectionStart;
	var currentEnd = obj.value.length - obj.selectionEnd;
    var keyCode = (evt.which) ? evt.which : event.keyCode;
    if (keyCode != 37 && keyCode != 39 && keyCode != 8 && keyCode != 46 && keyCode != 9 && keyCode != 35 &&
        keyCode != 36 && keyCode != 13 && keyCode != 188) {
        if (obj.value.indexOf('-') == 0) {
            var strvalue = obj.value.substring(1);
            var val = new BigNumber(strvalue.replace(/[^0-9,]/g, '').replace(',', '.')).toString();
            val = val.replace('.', ',')
            strvalue = '';
            for (var i = 0; i < val.length; i++) {
                if (val.charAt(i) != ',') {
                    strvalue += getVal(val.charAt(i));
                } else {
                    strvalue += val.charAt(i);
                }
            }
            strvalue = formatPhan(strvalue)
            obj.value = "-" + (strvalue);
        } else {
            var strvalue = obj.value;
            var val = strvalue;
            strvalue = '';
            for (var i = 0; i < val.length; i++) {
                if (val.charAt(i) != ',') {
                    strvalue += getVal(val.charAt(i));
                } else {
                    strvalue += val.charAt(i);
                }
            }
            obj.value = formatPhan(strvalue);
        }
    }
	var newStart = obj.value.length - currentStart;
	var newEnd = obj.value.length - currentEnd;
	obj.setSelectionRange(newStart, newEnd);
}
function validNumber(obj){
    var strValue = obj.value.toString();
    var numValue;
    if(strValue == '' || strValue == null){
        obj.value = '0';
    }
    else {
        if(strValue.indexOf('(') != -1){
            strValue = strValue.replace('(', '');
            strValue = strValue.replace(')', '');
            strValue = '-' + strValue;
        }
        while(strValue.indexOf('.') != -1) {
            strValue = strValue.replace('.', '');
        }

        if(strValue.length>15) strValue = strValue.substring(0,15);
        obj.value=formatNumber(numValue.toString());
    }

    return true;
}

function getVal(num){
if(num==''||checkNumeric(num)){
    return '';
}else{
    return (num);
    }
}
function formatNumber(pnumber){
    if(pnumber=='-0' ){
        pnumber ='0';  
    }
    var pnumberStr = pnumber;
    var len;
    var firstPartIndex;
    var remainPart;
    var returnValue;
    if(pnumber.indexOf('-') == 0) {
        pnumberStr = pnumber.substring(1);
    }
    if(pnumber.indexOf('.') > 0) {
        pnumberStr = pnumber.substring(0,pnumber.indexOf('.'));
    }
    len = pnumberStr.length;
    if (len < 4){
        returnValue = pnumberStr;
    } else {        
        firstPartIndex = len%3;
        returnValue = pnumberStr.substring(0, firstPartIndex);
        remainPart = pnumberStr.substring(firstPartIndex);
        len = remainPart.length;
    
        for(i = 0; i < len; i = i + 3) {
            returnValue = returnValue + "." + remainPart.substring(i, i + 3);
        }
        if(firstPartIndex == 0) {
            returnValue = returnValue.substring(1);
        }
    }
    if(pnumber.indexOf('-') == 0) {
        returnValue = '(' + returnValue + ')';
    }
    return returnValue; 
} 

function checkNumeric(sText)
{  
    var ValidChars = "0123456789";
    var IsNumber=true;
    if (ValidChars.indexOf(sText) != -1) 
      {
        IsNumber = false;
      }
    return IsNumber;
}
function formatPhan(number){   
        var num = number;
        if(num.length>16) num = num.substring(0,16);
        var currentVal = num;
        var s1 = testCommas(currentVal);
        var testDecimal = testDecimals(currentVal);
        if (testDecimal.length > 1) {
            currentVal = currentVal.slice(0, -1);
        }
        if(currentVal !=""){
        currentVal = replaceCommas(currentVal); 
        var s2 = testCommas(number);
        }else{
        currentVal = 0; 
        }
        return currentVal;
}


/////////////////----------------- end new
function testDecimals(currentVal) {
    var count;
    currentVal.match(/\,/g) === null ? count = 0 : count = currentVal.match(/\,/g).length;
    return count;
  }
  function testCommas(currentVal) {
    var count;
    currentVal.match(/\./g) === null ? count = 0 : count = currentVal.match(/\./g).length;
    return count;
  }
  
  function replaceCommas(yourNumber) {    
    var str = '';
    if(yourNumber.substr(0,1)=='-'){
        str = '-';
        yourNumber = yourNumber.substr(1);
    }
    var components = yourNumber.toString().split(",");
    if(components[0] != null && components[0] != ""){
      if (components.length === 1){
          components[0] = (components[0] != null && components[0] != "")? components[0]:"0";
          components[0] = components[0].toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
      if (components.length === 2){
          components[0] = (components[0] != null && components[0] != "")? components[0]:"0";
          components[0] = components[0].toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          components[1] = (components[1] != null && components[1] != "")? components[1]:"0";
          //components[1] = new BigNumber(components[1].toString().replace(/\D/g, "")).toString().substr(0,2);
          components[1] = components[1].toString().replace(/\D/g, "").toString().substr(0,2);
      }
      return str + components.join(",");
    }else{
      components = '';
      return str + components
    }    
  }
  // Get the start position of the caret in the object
function getCaretStart(obj){
	if(typeof obj.selectionStart != "undefined"){
		return obj.selectionStart;
	}else if(document.selection&&document.selection.createRange){
		var M=document.selection.createRange();
		try{
			var Lp = M.duplicate();
			Lp.moveToElementText(obj);
		}catch(e){
			var Lp=obj.createTextRange();
		}
		Lp.setEndPoint("EndToStart",M);
		var rb=Lp.text.length;
		if(rb>obj.value.length){
			return -1;
		}
		return rb;
	}
}
function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode;
       if(charCode == 59 || charCode == 46)
        return false;
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
       return true;
    }
function isNumberKey2(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode;
       if(charCode == 59 || charCode == 46)
        return true;
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
       return true;
    }
// sets the caret position to l in the object
function setCaret(obj,l){
	obj.focus();
	if (obj.setSelectionRange){
		obj.setSelectionRange(l,l);
	}else if(obj.createTextRange){
		m = obj.createTextRange();		
		m.moveStart('character',l);
		m.collapse();
		m.select();
	}
}

    function formatNum(elt){
        var position = getCaretStart(elt);
        var currentVal = elt.value;
        var s1 = testCommas(currentVal);
        var testDecimal = testDecimals(currentVal);
        if (testDecimal.length > 1) {
            currentVal = currentVal.slice(0, -1);
        }
        if(currentVal!=""){
        elt.value = replaceCommas(currentVal); 
        var s2 = testCommas(elt.value);
        setCaret(elt, position + (s2 - s1));
        }else{
        elt.value = ''; 
        }
  }
    function formatNum1(elt){
        var position = getCaretStart(elt);
        var testCharcode = elt.value.split(",");
        if(testCharcode.length > 2){
          var indexOfFirt =  elt.value.indexOf(",");
          elt.value = elt.value.substring(0,elt.value.indexOf(",", indexOfFirt+1));
        }
        var currentVal = elt.value.toString().replace(/[^0-9,]/g, '');
        var s1 = testCommas(currentVal);
        var testDecimal = testDecimals(currentVal);
        if (testDecimal.length > 1) {
            currentVal = currentVal.slice(0, -1);
        }
        if(currentVal!=""){
        elt.value = replaceCommas(currentVal); 
        var s2 = testCommas(elt.value);
        setCaret(elt, position + (s2 - s1));
        }else{
          elt.value='';
        }
  }
  function reLog()
	{
    
		if(showConfirm("SYS-0011")==1){
			var sFeatures = 'resizable=yes,status=yes,toolbar=no,menubar=no,scrollbars=yes,width=800px,height=600px,top=0,left=0';
			var newWindow = window.open("/reLog.do?reLog=true","_parent",sFeatures);	
		}
	}
	/**
	  *  Log ra trang màn hình chính
	  */
	function closeApp(){
		if (confirm("Bạn có thực sự muốn thoát ra kh�?i hệ thống?"))
		{
			var sFeatures = 'resizable=yes,status=yes,toolbar=no,menubar=no,scrollbars=yes,width=800px,height=600px,top=0,left=0';
			var newWindow = window.open("/logout.do?reLog=false","_parent",sFeatures);	
		}
	}
	/**
	  *  Thoát hẳn ra kh�?i ứng dụng
	  */
	function exitApp(){
		if (confirm("Bạn có chắc chắn muốn thoát khỏi hệ thống?"))
		{
			var sFeatures = 'resizable=yes,status=yes,toolbar=no,menubar=no,scrollbars=yes,width=800px,height=600px,top=0,left=0';
			var newWindow = window.open("/logout.do","_parent",sFeatures);	
		}
	}
  function textfocus(obj){
      obj.style.backgroundColor='#ffffb5';
  }
  function textlostfocus(obj){
      obj.style.backgroundColor='#ffffff';
  }    
  function onkeydown(e){
      if (!e) e = window.event;
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 188]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
  }  
  function onkeydownnodot(e){
      if (!e) e = window.event;
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
  }  
  function toNumber(strNumber){
    var numStr = strNumber;
    if (null == strNumber || '' == strNumber) {
        return 0;
    }
    
    while(numStr.indexOf('.') != -1) {
        numStr = numStr.replace('.', '');
    }
    while(numStr.indexOf(',') != -1) {
        numStr = numStr.replace(',', '.');
    }
    if(numStr.indexOf('(') != -1) {
        numStr = numStr.replace('(', '');
        numStr = numStr.replace(')', '');
        numStr = '-' + numStr;
    }
    return parseFloat(numStr);
}
  
  function blockKeySpecial001(e) {
      //      e.keyCode
      var code;
      if (!e)
          var e = window.event;
      if (e.keyCode)
          code = e.keyCode;
      else if (e.which)
          code = e.which;
      var character = String.fromCharCode(code);
      var pattern = /[!@#$%^&*()_+='"\[\]\:\;\{\}\<\>\?]/;
      if (pattern.test(character)) {
          character.replace(character, "");
          return false;
      }
      else {
          return true;
      }
  }
  function blockKeyDT(e) {
      //      e.keyCode
      var code;
      if (!e)
          var e = window.event;
      if (e.keyCode)
          code = e.keyCode;
      else if (e.which)
          code = e.which;
      var character = String.fromCharCode(code);
      var pattern = /[!@#$%^&*()_+='"\[\]\.\,\:\;\{\}\<\>\?a-zA-Za-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/;
      if (pattern.test(character)) {
          character.replace(character, "");
          return false;
      }
      else {
          return true;
      }
  }
  
  function blockKeyNumber(e) {
      //      e.keyCode
      var code;
      if (!e)
          var e = window.event;
      if (e.keyCode)
          code = e.keyCode;
      else if (e.which)
          code = e.which;
      var character = String.fromCharCode(code);
      var pattern = /[!@#$%^&*()_+='"\[\]\.\:\;\{\}\<\>\?\ \/\|\;\'a-zA-Za-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/;
      if (pattern.test(character)) {
          character.replace(character, "");
          return false;
      }
      else {
          return true;
      }
  }
  function format(elt){
    var currentVal = elt.value;
    if(currentVal.length ==3){
      elt.value = elt.value +"/";
    }
  }
  
//  function allowPressNeg(obj) {  
//    var dsChar = '-';
//    if (obj.value.indexOf(dsChar) >= 0) {
//        if((event.keyCode < 48 && event.keyCode != 9) || event.keyCode > 57) event.returnValue = false;
//    }else{
//        if((event.keyCode < 48 && event.keyCode != 9 && event.keyCode != 45) || event.keyCode > 57) event.returnValue = false;
//    }
//}
    function validateFloatKeyPress(el, evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
		console.log(charCode);
        var dsChar = '-';
        if (charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57)&& charCode!=188 && charCode != 45) {
               return false;
        }
        if (el.value.indexOf(dsChar) >= 0) {              
              if(testDecimals(el.value) ==0){
                if (charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57) ) {
                    return false;
                }
              }else {
                  if(testDecimals(el.value) ==1){
                      if (charCode == 44 && charCode > 31 && (charCode < 48 || charCode > 57)&& charCode!=188 ) {
                      return false;
                  }
                }
              }
        }else{              
            if(testDecimals(el.value) ==0){
              if (charCode != 44 && charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
                  return false;
              }
            }else {
                if(testDecimals(el.value) ==1){
                    if (charCode == 44 && charCode > 31 && (charCode < 48 || charCode > 57)&& charCode!=188 && charCode != 45) {
                    return false;
                }
              }
            }
        }
        
        if(charCode ==47){
          var str = el.value;
          if(str.length==1){
            str="0"+"0"+str;
            el.value=str;
          }else if(str.length==2){
            str="0"+str;
            el.value=str;
          }else if(str.length >= 4){
            
            return false;
          }
        }
        return true;
  }
	function validateLongKeyPress(el, evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		console.log(charCode);
		var dsChar = '-';
		if ((charCode < 48 || charCode > 57) && charCode !=45) {
			return false;
		}
		if (el.value.indexOf(dsChar) >= 0) {
			if (charCode < 48 || charCode > 57) {
				return false;
			}
		} else {
			if ((charCode < 48 || charCode > 57) && charCode !=45) {
				return false;
			}
		}
		return true;
	}
  function upperCase(obj){
     obj.value = obj.value.toUpperCase()
  }
  var format = "mm/dd/yyyy";
  var match = new RegExp(format
    .replace(/(\w+)\W(\w+)\W(\w+)/, "^\\s*($1)\\W*($2)?\\W*($3)?([0-9]*).*")
    .replace(/m|d|y/g, "\\d"));
  var replace = "$1/$2/$3$4".replace(/\//g, format.match(/\W/));
  function doFormat(e) {
      if (!e) e = window.event;
      if(!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
      var target = e.target || e.srcElement;
      if(target!=null){
        target.value = target.value.replace(/(^|\W)(?=\d\W)/g, "$10").replace(match, replace).replace(/(\W)+/g, "$1");      
      }
  }
    function validatedate(inputText)  
      {  
      var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;  
      // Match the date format through regular expression  
      if(inputText.value.match(dateformat))  
      {  
      inputText.focus();  
      //Test which seperator is used '/' or '-'  
      var opera1 = inputText.value.split('/');  
      var opera2 = inputText.value.split('-');  
      lopera1 = opera1.length;  
      lopera2 = opera2.length;  
      // Extract the string into month, date and year  
      if (lopera1>1)  
      {  
      var pdate = inputText.value.split('/');  
      }  
      else if (lopera2>1)  
      {  
      var pdate = inputText.value.split('-');  
      }  
      var dd = parseInt(pdate[0]);  
      var mm  = parseInt(pdate[1]);  
      var yy = parseInt(pdate[2]);  
      // Create list of days of a month [assume there is no leap year by default]  
      var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
      if (mm==1 || mm>2)  
      {  
      if (dd>ListofDays[mm-1])  
      {  
      alert('Sai định dạng ngày tháng !');  
      return false;  
      }  
      }  
      if (mm==2)  
      {  
      var lyear = false;  
      if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
      {  
      lyear = true;  
      }  
      if ((lyear==false) && (dd>=29))  
      {  
      alert('Sai định dạng ngày tháng !');  
      return false;  
      }  
      if ((lyear==true) && (dd>29))  
      {  
      alert('Sai định dạng ngày tháng !');  
      return false;  
      }  
      }  
      }  
      else  
      {  
      alert("Sai định dạng ngày tháng !");  
      inputText.focus();  
      return false;  
      }  
      return true;
      }  
      function validatetime(inputText) {
        var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        var dateformat2 = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4} (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/;
        
        // Match the date format through regular expression  
        if (inputText.match(dateformat) || inputText.match(dateformat2)) {
            //Test which seperator is used '/' or '-'  
            var opera1 = inputText.split('/');
            var opera2 = inputText.split('-');
            lopera1 = opera1.length;
            lopera2 = opera2.length;
            // Extract the string into month, date and year  
            if (lopera1 > 1) {
                var pdate = inputText.split('/');
            }
            else if (lopera2 > 1) {
                var pdate = inputText.split('-');
            }
            var dd = parseInt(pdate[0]);
            var mm = parseInt(pdate[1]);
            var yy = parseInt(pdate[2].substring(0,4));
            // Create list of days of a month [assume there is no leap year by default]  
            var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (mm == 1 || mm > 2) {
                if (dd > ListofDays[mm - 1]) {
                    return false;
                }
            }
            if (mm == 2) {
                var lyear = false;
                if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                    lyear = true;
                }
                if ((lyear == false) && (dd >= 29)) {
                    return false;
                }
                if ((lyear == true) && (dd > 29)) {
                    return false;
                }
            }
        }
        else {
            return false;
        }
        return true;
    }
    
    function changeMenu(){
      var typeMenu = document.getElementById("TypeMenu").value;
       $.ajax( {
          type : "get",
          url : "getlsmenu?typeMenu="+typeMenu,
          data:"",
          async : true, cache : false, success : function (data) {
           //   var obj = JSON.parse(data);
              window.location.href = "main";
          }});
    }
</SCRIPT>

	<TABLE border=0 cellSpacing=0 cellPadding=0 width=985 align=center>
		<TBODY>
			<TR>
				<TD height=70>
					<a href="main"><IMG src="styles/images/Untitled-1.jpg" width=985 height=70></a>
				</TD>
			</TR>
		</TBODY>
	</TABLE>

	<div class="container padding0">
		<ul class="top-ttcn">
			<li><span class="glyphicon" aria-hidden="true"></span>
				<a href="qlldcnq"> QLLĐCNQ </a></li>
			<li><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
				<a href="logout"> Tho&#225;t </a></li>
		</ul>
	</div>
	<script language="JavaScript">
  $(function(){
  $('.topmenu > li > ul > li > ul').parent().addClass('themmuiten');
  $('.topmenu > li > ul > li > ul > li > ul').parent().addClass('themmuiten');
  })
</script>
	<div class="container padding0 main">