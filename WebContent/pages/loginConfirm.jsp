<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>  
<LINK rel="stylesheet" type="text/css" href="styles/css/style.css" media="screen">
<LINK rel="stylesheet" type="text/css" href="styles/css/khobac.css" media="screen">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <title>loginConfirm</title>
    
  </head>
  <body>    
    <s:form action="loginAction">
    <table width="100%">
      <tr>
        <td>&nbsp;<br/><br/><br/><br/><br/><br/><br/></td>
      </tr>
      <tr>
        <td width="10%"></td>        
        <td>
          <table width="100%" border="0">
            <tr>
              <div class="app_error">                  
                     <div class="welcome">
                        <td align="center"><font size="4"><b><font color="Red"> C&#7843;nh b&#225;o: </font><br/>M&#225;y c&#243; &#273;&#7883;a ch&#7881; IP: <font color="Red"> <%=request.getAttribute("ip_dang_truycap")%> </font> &#273;ang truy c&#7853;p v&#224;o NSD: <font color="Red"><s:property value="loginForm.ma_nsd"/></font> <br/>B&#7841;n c&#243; mu&#7889;n ti&#7871;p t&#7909;c &#273;&#259;ng nh&#7853;p?</b></font></td>
                     </div>
               </div>
            </tr>
          </table>
        </td>
        <td width="10%"></td>
      </tr>
      <tr>
        <td width="10%">&nbsp;<br/><br/><br/><br/></td>
        <td align="center">
          <button type="button" style="width: 100px" id="btnYes" onclick="onSubmit('yes');" accesskey="c"
                            class="ButtonCommon">
                      <span class="sortKey">C</span>ó
          </button>
          <button type="button" style="width: 100px" id="btnNo" onclick="onSubmit('no');" accesskey="k"
                            class="ButtonCommon">
                      <span class="sortKey">K</span>hông
          </button>
        </td>
        <td width="10%"></td>
      </tr>
    </table>
      <s:hidden name="loginForm.ip_truycap"/>
      <s:hidden name="loginForm.ma_nsd"/>
      <s:hidden name="loginForm.mat_khau"/>
      <s:hidden name="loginForm.domain"/>
      <s:hidden name="loginForm.mac_address"/>
      <s:hidden name="loginForm.user_may_truycap"/>
      <s:hidden name="loginForm.ten_may_truycap"/>
      <s:hidden name="loginForm.login_status" value="confirm"/>            
    </s:form>  
  </body>
</html>
<script type="text/javascript">
  document.getElementById('btnYes').focus();
  function onSubmit(type){
    var f = document.forms[0];
    if( type == 'yes'){
      f.action = 'loginAction';      
    }else{
      f.action = 'login';
    }
    f.submit();
  }
</script>


