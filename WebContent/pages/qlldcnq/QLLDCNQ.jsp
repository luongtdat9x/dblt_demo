<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:include page="/includes/tpcp_header_phanquyen.jsp"></jsp:include>
<script src="angularjs/vendors/moment/moment.min.js"></script>
<script src="script/qlldcnq/QlldcnqController.js"></script>
<script src="styles/js/MoneyConvert_vn.js"></script>
<script src="script/qlldcnq/QlldcnqService.js"></script>
<script type="text/javascript">
  $(function () {
        $("#txtLenhTu").datepicker( {
          dateFormat : "dd/mm/yy"
        });    
  });
  $(function () {
        $("#txtLenhDen").datepicker( {
          dateFormat : "dd/mm/yy"
        });    
  });
</script>
	<div id="content" ng-controller="QlldcnqController">
    <div style="display:{{displayList}}">
			<div class="panel-heading border-bottom">
				<h1 class="panel-title">
					<strong>
        QUẢN LÝ LỆNH ĐIỀU CHUYỂN NGÂN QUỸ
					</strong>
				</h1>
			</div>
			<div class="app_error_list">
        <div uib-alert ng-repeat="alert in listAlerts" ng-class="'alert-' + (alert.type || 'warning')" close="listCloseAlert($index)"> {{alert.errMessage}}</div>
			</div>
			<!-- tim kiem-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title">
            Tìm kiếm
					</h2>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số lệnh</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoLenh" name="txtSoLenh"  
                   placeholder="Số lệnh" list="listSL" ng-model="search.soLenh"></input>
                   <datalist id="listSL">
                        <option ng-repeat="lenh in listLenhs" value="{{lenh.soLenh}}">{{lenh.soLenh}}</option>
                    </datalist>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Năm</label>
								<div class="col-sm-8">
                   <input class="form-control" list="listNam" type="" ng-model="search.nam" placeholder="Chọn năm" 
                   onkeypress="return isNumberKey(event)" maxlength="4"/>
                    <datalist id="listNam">
                        <option ng-repeat="nam in listNams" value="{{nam.nam}}"></option>
                    </datalist>
								</div> 
							</div>
						</div>
						<div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Ngân hàng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="search.idNHTM">
                     <option value="">Tất cả</option>
                     <option ng-repeat="nganHang in listNHTMs" 
                             value="{{nganHang.idNHTM}}">{{nganHang.tenNHTM}}</option>
                    </select>
								</div> 
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Trạng thái</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="search.trangThai">
                     <option value="">Tất cả</option>
                     <option ng-repeat="trangThai in trangThais" 
                             value="{{trangThai.key}}">{{trangThai.value}}</option>
                    </select>
								</div>
							</div>
						</div>
            <div class="row">
              <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Ngày lập lệnh từ</label>
               <div class="col-sm-8">
                <div class="input-group date">
                <input class="form-control" type="text" id="txtLenhTu" name="txtLenhTu" min="1900" max="3000" placeholder="dd/MM/yyyy"
                        ng-model="search.ngayLenh1" onfocus="textfocus(this);" onkeyup="doFormat(event)" onblur="textlostfocus(this);" maxlength="10">
                </input>
                <label class="input-group-addon" for="txtLenhTu"> 
                      <span class="glyphicon glyphicon-calendar" ></span>
                </label>
                </div>
                <span class="error" style="color:red" ng-show="emptyFromDate">Bạn nhập từ ng&agrave;y kh&ocirc;ng hợp lệ</span>
               </div>
              </div>
              <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Ngày lập lệnh đến</label>
               <div class="col-sm-8">
                <div class="input-group date">
                <input class="form-control" type="text" id="txtLenhDen" name="txtLenhDen" min="1900" max="3000" placeholder="dd/MM/yyyy"
                        ng-model="search.ngayLenh2" onfocus="textfocus(this);" onkeyup="doFormat(event)" onblur="textlostfocus(this);" maxlength="10">
                </input>
                <label class="input-group-addon" for="txtLenhDen"> 
                      <span class="glyphicon glyphicon-calendar" ></span>
                </label>
                </div>
                <span class="error" style="color:red" ng-show="emptyToDate">Bạn nhập đến ng&agrave;y kh&ocirc;ng hợp lệ</span>
                        <div ng-show="errorDate" class="text-danger">
                           <span class="error" style="color:red">Từ ng&agrave;y phải nhỏ hơn đến ng&agrave;y</span>
                        </div>
               </div>
              </div>
						</div>
    
					</div>
				</div>
				<div class="button-group center">
          <button id="them" type="button"  ng-click="viewAdd()"  
              class="btn btn-default" accesskey="m">
                      Th&ecirc;<span class="sortKey">m</span> mới
					</button>
					
          <button id="tracuu" type="button" ng-click="find()"
              class="btn btn-default" accesskey="t">
						<span class="sortKey">T</span>ra cứu
					</button>

					<button id="thoat" type="button" ng-click="thoat()"
              class="btn btn-default" accesskey="o">
                      Th<span class="sortKey">o</span>&aacute;t
					</button>
				</div>
			</div>
			<!-- end tim kiem-->

			<div class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title">
        Kết quả: {{totalItems}} bản ghi.
					</h2>
				</div>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
              <th>STT</th>
							<th>Số lệnh</th>
							<th>Phụ lục HĐ</th>
							<th>Ngân hàng</th>	
							<th>Số tiền (tỷ đồng)</th>
              <th>Ngày lập lệnh</th>
              <th>Ngày thực hiện lệnh</th>
              <th>Trạng thái</th>
              <th>Tình trạng báo cáo</th>
              <th>Sửa</th>
              <th>Xóa</th>
						</tr>
					</thead>

					<tbody>
                                        <tr></tr>
						<tr class="odd gradeX" ng-repeat="lenh in listLenhs">
              <td class="center">{{lenh.rn}}</td>
              <td><a ng-click="view($index)">{{lenh.soLenh}}</a></td>
							<td><span ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id==lenh.idPL">{{phuLuc.name}}</span></td>
              <td>{{lenh.maNHTM}}-{{lenh.tenNHTM}}</td>
              <td class="right">{{lenh.soTien | dispNumber}}</td>
              <td class="center">{{lenh.ngayTao}}</td>
              <td class="center">{{lenh.ngayThucHien}}</td>
              <td>
                <span ng-repeat="trangThai in trangThais" ng-if="lenh.trangThai==trangThai.key">{{trangThai.value}}</span>
              </td>
              <td>
                <span ng-repeat="tinhTrang in tinhTrangBCs" ng-if="lenh.ttBC==tinhTrang.key">{{tinhTrang.value}}</span>
              </td>
							<td class="center">
                  <button class="btn btn-primary" ng-click="edit($index)" ng-if="lenh.trangThai == '01' || lenh.trangThai == '04'">
                    <i class="glyphicon glyphicon-pencil"></i>
                  </button>
              </td> 
              <td class="center">
								<button class="btn btn-danger" ng-click="delt(lenh.id, $index)" ng-if="lenh.trangThai == '01' || lenh.trangThai == '04'">
									<i class="glyphicon glyphicon-remove"></i>
								</button>
							</td>
              
						</tr>
					</tbody>

					<tfoot>
          
						<tr>
                <td colspan="12">
                <ul uib-pagination total-items="totalItems" 
                      ng-model="currentPage" 
                      max-size="7" ng-change="pageChanged()"
                      class="pagination-sm"
                      boundary-link-numbers="true"></ul>
                </td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
    
    <!-- LẬP LỆNH ĐIỀU CHUYỂN QUỸ -->
    <div style="display:{{displayAdd}}">
      <form class="form-horizontal" id="formAdd" name="formAdd">
      <div class="panel-heading border-bottom">
				<h1 class="panel-title">
					<strong>
        LẬP LỆNH ĐIỀU CHUYỂN NGÂN QUỸ
					</strong>
				</h1>
			</div>
			<div class="app_error" id ="detail_error">
        <div uib-alert ng-repeat="alert in detailAlerts" ng-class="'alert-' + (alert.type || 'warning')" close="detailCloseAlert($index)"> {{alert.errMessage}}</div>
			</div>
			<div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chung
					</h2>
				</div>
				<div class="panel-body">
        
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Ngày lập lệnh:</b></div>
								<div class="col-sm-8" ng-model="dbLenh.ngayTao"><b>{{dbLenh.ngayTao}}</b></div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"><b>Tài khoản chuyển:</b></label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idTKC" ng-change="selectKBNN()"
                    id="txtTKC" name="txtTKC">
                     <option value="">-- Chọn tài khoản --</option>
                     <option ng-repeat="tkKBNN in listTKKBNNs" 
                             value="{{tkKBNN.id}}">{{tkKBNN.soTK}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Tên đơn vị chuyển:</b></div>
								<div class="col-sm-8"><b>{{donViKBNN}}</b></div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Địa chỉ:</b></div>
								<div class="col-sm-8"><b>{{diaChiKBNN}}</b></div>
							</div>
						</div>
            
        </div>
			</div>
      
      <div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chi tiết
					</h2>
				</div>
				<div class="panel-body" style="overflow: hidden;">
        
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số lệnh</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoLenh" name="txtSoLenh"  
                   placeholder="Số lệnh" ng-model="dbLenh.soLenh" disabled></input>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Phụ lục hợp đồng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idPL"
                    id="txtPL" name="txtPL" ng-change="selectPL()">
                     <option value="">-- Chọn hợp đồng --</option>
                     <option ng-repeat="phuLuc in phuLucs" 
                             value="{{phuLuc.id}}">{{phuLuc.name}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Ngân hàng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idNHTM" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.idNHTM}}">{{phuLuc.maNHTM}}</option>
                    </select>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tài khoản nhận</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="soTaiKhoanNhan" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.soTK}}">{{phuLuc.soTK}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Tên tài khoản nhận</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNameTKNhan" name="txtNameTKNhan" disabled ng-model="tenTKN"></input>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Nơi mở tài khoản</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNoiMoTK" name="txtNoiMoTK" ng-model="noiMoTKN" disabled></input>
								</div>
							</div>
						</div>
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tiền (Tỷ đồng)</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoTien" name="txtSoTien" 
                  onfocus="this.value = formatNumOnfocus(this.value);" onkeypress="return validateFloatKeyPress(this,event);" maxlength="14"
                  onkeyup="formatSoAm(this,event);this.value=this.value.replace(/,/,'');
                                    this.value=this.value.substring(this.value.length - 14)"
                                    onblur="formatNumberBlur(this);" style="text-align:right;"
                  ng-model="dbLenh.soTien" disabled/>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Số tiền <i>(Bằng chữ)</i></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="txtSoTienC" name="txtSoTienC" 
                  ng-model="soTienBangChu" disabled style="width: 96%"></input>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Nội dung thanh toán</label>
								<div class="col-sm-10">
									<textarea class="form-control" type="text" id="txtNDTT" name="txtNDTT" style="overflow-y:hidden;width:96%;height:72px;"
                  ng-model="dbLenh.ndtt" maxlength="250"></textarea>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group">
                <div class="button-group center">
                  <button type="button" class="btn btn-default" ng-disabled="checkButton" ng-click="add('luu')">Lưu</button>
                  <button type="button" class="btn btn-default" ng-disabled="checkButton" ng-click="add('duyet')">Trình phê duyệt</button>
                  <button type="button" class="btn btn-default" ng-click="back()"
                  accesskey="t"><span class="sortKey">T</span>hoát</button>
                </div>
              </div>
            </div>
          </div>  
				</div>
      
      </form>
		</div>
    
    <!-- SỬA LỆNH ĐIỀU CHUYỂN QUỸ -->
    <div style="display:{{displayEdit}}">
      <form class="form-horizontal" id="formEdit" name="formEdit">
      <div class="panel-heading border-bottom">
				<h1 class="panel-title">
					<strong>
        SỬA LỆNH ĐIỀU CHUYỂN NGÂN QUỸ
					</strong>
				</h1>
			</div>
			<div class="app_error" id ="detail_error">
        <div uib-alert ng-repeat="alert in detailAlerts" ng-class="'alert-' + (alert.type || 'warning')" close="detailCloseAlert($index)"> {{alert.errMessage}}</div>
			</div>
			<div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chung
					</h2>
				</div>
				<div class="panel-body">
        
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Ngày lập lệnh:</b></div>
								<div class="col-sm-8" ng-model="dbLenh.ngayTao"><b>{{dbLenh.ngayTao}}</b></div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label"><b>Tài khoản chuyển:</b></label>
								<div class="col-sm-8">
                    <select class="form-control" ng-selected="dbLenh.idTKC"
                    ng-model="dbLenh.idTKC" ng-change="selectKBNN()">
                     <option value="">-- Chọn tài khoản --</option>
                     <option ng-repeat="tkKBNN in listTKKBNNs" 
                             value="{{tkKBNN.id}}">{{tkKBNN.soTK}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Tên đơn vị chuyển:</b></div>
								<div class="col-sm-8"><b>{{donViKBNN}}</b></div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Địa chỉ:</b></div>
								<div class="col-sm-8"><b>{{diaChiKBNN}}</b></div>
							</div>
						</div>
            
        </div>
			</div>
      
      <div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chi tiết
					</h2>
				</div>
				<div class="panel-body" style="overflow: hidden;">
        
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số lệnh</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoLenh" name="txtSoLenh"  
                   placeholder="Số lệnh" ng-model="dbLenh.soLenh" disabled></input>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Phụ lục hợp đồng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idPL" ng-change="selectPL()">
                     <option value="">-- Chọn hợp đồng --</option>
                     <option ng-repeat="phuLuc in phuLucs" 
                             value="{{phuLuc.id}}">{{phuLuc.name}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Ngân hàng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idNHTM" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.idNHTM}}">{{phuLuc.maNHTM}}</option>
                    </select>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tài khoản nhận</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="soTaiKhoanNhan" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.soTK}}">{{phuLuc.soTK}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Tên tài khoản nhận</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNameTKNhan" name="txtNameTKNhan" ng-model="tenTKN" disabled></input>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Nơi mở tài khoản</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNoiMoTK" name="txtNoiMoTK" ng-model="noiMoTKN" disabled></input>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tiền (Tỷ đồng)</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoTien" name="txtSoTien" 
                  onfocus="this.value = formatNumOnfocus(this.value);" onkeypress="return validateFloatKeyPress(this,event);" maxlength="14"
                  onkeyup="formatSoAm(this,event);this.value=this.value.replace(/,/,'');
                                    this.value=this.value.substring(this.value.length - 14)"
                                    onblur="formatNumberBlur(this);" style="text-align:right;"
                  ng-model="dbLenh.soTien" disabled/>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Số tiền <i>(Bằng chữ)</i></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="txtSoTienC" name="txtSoTienC" 
                  ng-model="soTienBangChu" disabled style="width: 96%"></input>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Nội dung thanh toán</label>
								<div class="col-sm-10">
									<textarea class="form-control" type="text" id="txtNDTT" name="txtNDTT" style="overflow-y:hidden;width:96%;height:72px;"
                  ng-model="dbLenh.ndtt" maxlength="250"></textarea>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group">
                <div class="button-group center">
                  <button type="button" class="btn btn-default" ng-disabled="checkButton" ng-click="save('luu')">Lưu</button>
                  <button type="button" class="btn btn-default" ng-disabled="checkButton" ng-click="save('duyet')">Trình phê duyệt</button>
                  <button type="button" class="btn btn-default" ng-click="back()"
                  accesskey="t"><span class="sortKey">T</span>hoát</button>
                </div>
              </div>
            </div>
          </div>  
				</div>
        
      </form>
		</div>
    
    <!-- XEM LỆNH ĐIỀU CHUYỂN QUỸ -->
    <div style="display:{{displayView}}">
      <form class="form-horizontal" id="formView" name="formView">
      <div class="panel-heading border-bottom">
				<h1 class="panel-title">
					<strong>
        CHI TIẾT LỆNH ĐIỀU CHUYỂN NGÂN QUỸ
					</strong>
				</h1>
			</div>
			<div class="app_error" id ="detail_error">
        <div uib-alert ng-repeat="alert in detailAlerts" ng-class="'alert-' + (alert.type || 'warning')" close="detailCloseAlert($index)"> {{alert.errMessage}}</div>
			</div>
			<div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chung
					</h2>
				</div>
				<div class="panel-body">
        
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Ngày lập lệnh:</b></div>
								<div class="col-sm-8" ng-model="dbLenh.ngayTao"><b>{{dbLenh.ngayTao}}</b></div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Tài khoản chuyển:</b></div>
								<div class="col-sm-8"><b>{{dbLenh.soTKC}}</b></div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Tên đơn vị chuyển:</b></div>
								<div class="col-sm-8"><b>{{donViKBNN}}</b></div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
                <div class="col-sm-4 right"><b>Địa chỉ:</b></div>
								<div class="col-sm-8"><b>{{diaChiKBNN}}</b></div>
							</div>
						</div>
            
        </div>
			</div>
      
      <div class="panel panel-default">
        <div class="panel-heading">
					<h2 class="panel-title">
            Thông tin chi tiết
					</h2>
				</div>
				<div class="panel-body" style="overflow: hidden;">
        
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số lệnh</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoLenh" name="txtSoLenh"  
                   placeholder="Số lệnh" ng-model="dbLenh.soLenh" disabled></input>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Phụ lục hợp đồng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idPL" ng-change="selectPL()" disabled>
                     <option value="">-- Chọn hợp đồng --</option>
                     <option ng-repeat="phuLuc in phuLucs" 
                             value="{{phuLuc.id}}">{{phuLuc.name}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Ngân hàng</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="dbLenh.idNHTM" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.idNHTM}}">{{phuLuc.maNHTM}}</option>
                    </select>
								</div>
							</div>
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tài khoản nhận</label>
								<div class="col-sm-8">
                    <select class="form-control" ng-model="soTaiKhoanNhan" disabled>
                     <option value=""></option>
                     <option ng-repeat="phuLuc in phuLucs" ng-selected="phuLuc.id == dbLenh.idPL"
                             value="{{phuLuc.soTK}}">{{phuLuc.soTK}}</option>
                    </select>
								</div>
							</div>
						</div>
            
            <div class="row">
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Tên tài khoản nhận</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNameTKNhan" name="txtNameTKNhan" disabled ng-model="tenTKN"></input>
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Nơi mở tài khoản</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtNoiMoTK" name="txtNoiMoTK" disabled ng-model="noiMoTKN"></input>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-6">
								<label class="col-sm-4 control-label">Số tiền (Tỷ đồng)</label>
								<div class="col-sm-8">
									<input class="form-control" type="text" id="txtSoTien" name="txtSoTien" 
                  onfocus="this.value = formatNumOnfocus(this.value);" onkeypress="return validateFloatKeyPress(this,event);" maxlength="14"
                  onkeyup="formatSoAm(this,event);this.value=this.value.replace(/,/,'');
                                    this.value=this.value.substring(this.value.length - 14)"
                                    onblur="formatNumberBlur(this);" style="text-align:right;"
                  ng-model="dbLenh.soTien" ng-change="docSoTien(dbLenh.soTien)" disabled/>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Số tiền <i>(Bằng chữ)</i></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="txtSoTienC" name="txtSoTienC" 
                  ng-model="soTienBangChu" disabled style="width: 96%"></input>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Nội dung thanh toán</label>
								<div class="col-sm-10">
									<textarea class="form-control" type="text" id="txtNDTT" name="txtNDTT" style="overflow-y:hidden;width:96%;height:72px;"
                  ng-model="dbLenh.ndtt" disabled></textarea>
								</div>
							</div>
						</div>
            
            <div class="row" ng-show="dbLenh.trangThai == '04' || dbLenh.trangThai == '08'">
              <div class="form-group col-sm-12" style="padding-left: 10px;">
								<label class="col-sm-2 control-label">Lý do từ chối</label>
								<div class="col-sm-10">
									<textarea class="form-control" type="text" id="txtLDTC" name="txtLDTC" style="overflow-y:hidden;width:96%;height:72px;"
                  ng-model="dbLenh.ldTC" disabled></textarea>
								</div>
							</div>
						</div>
            
            <div class="row">
              <div class="form-group">
                <div class="button-group center">
                  <button type="button" class="btn btn-default" ng-click="back()"
                  accesskey="t"><span class="sortKey">T</span>hoát</button>
                </div>
              </div>
            </div>
          </div>  
				</div>
  
      </form>
		</div>
    
       <!------------log------------->
   <div style="display:{{displayLog}}">
      <div class="row">
         <div class="form-group col-sm-6">
            <button type="button" style="border: 0; background: transparent"
               ng-click="displayDetailLog='none'">
            <img src="styles/images/u883.png" width="15" heght="15" alt="Hide log"/>
            </button>
            <a ng-click="log()"> 
            <i>Lịch sử thao t&aacute;c</i></a>
         </div>
      </div>
      <div style="display:{{displayDetailLog}}">
        <form class="form-horizontal">
         <div class="panel panel-default">
         <div class="panel-heading">
            <h1 class="panel-title">Danh sách lịch sử</h1>
         </div>
         <div class="panel-body" style="overflow:unset">
            <div ng-if="logFlag">
              <table class="table table-striped table-bordered">
                 <thead>
                    <tr>
                       <th>STT</th>
                       <th>Thao t&aacute;c</th>
                       <th>Người thực hiện</th>
                       <th>IP thực hiện</th>
                       <th>Ng&agrave;y thực hiện</th>
                       <th>Mô tả</th>
                       <th>Xem</th>
                    </tr>
                 </thead>
                 <tbody>
                 <tr></tr>
                    <tr class="odd gradeX" ng-repeat="log in lsLog">
                       <td class="center">{{$index + 1}}</td>
                       <td>
                          <span ng-repeat="manipulation in collManipulation"
                                ng-if="manipulation.key==log.log_type">{{manipulation.value}}</span>
                       </td>
                       <td>{{log.user_code}}</td>
                       <td>{{log.ip_address}}</td>
                       <td class="center">{{log.log_date}}</td>
                       <td>{{log.description}}</td>
                       <td class="center">
                          <a ng-click="viewDetailLog($index)" href="">Chi tiết</a>
                       </td>
                    </tr>
                 </tbody>
              </table>
            </div>
            <div ng-if="detailLogFlag">
                <form class="form-horizontal" id="formView" name="formView">
                <div class="app_error" id ="detail_error">
                  <div uib-alert ng-repeat="alert in detailAlerts" ng-class="'alert-' + (alert.type || 'warning')" close="detailCloseAlert($index)"> {{alert.errMessage}}</div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h2 class="panel-title">
                      Thông tin chung
                    </h2>
                  </div>
                  <div class="panel-body">
                  
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right"><b>Ngày lập lệnh:</b></div>
                          <div class="col-sm-7" ng-model="dbLenh.ngayTao"><b>{{dbLenh.ngayTao}}</b></div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right"><b>Tài khoản chuyển:</b></div>
                          <div class="col-sm-7">
                              <div ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhNew.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.soTK}}</b></div>
                              <div ng-show="dbLenhNew.ID_TAIKHOAN_CHUYEN != dbLenhOld.ID_TAIKHOAN_CHUYEN"
                              style="color: red" ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhOld.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.soTK}}</b></div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right"><b>Tên đơn vị chuyển:</b></div>
                          <div class="col-sm-7">
                              <div ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhNew.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.tenDonVi}}</b></div>
                              <div ng-show="dbLenhNew.ID_TAIKHOAN_CHUYEN != dbLenhOld.ID_TAIKHOAN_CHUYEN"
                              style="color: red" ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhOld.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.tenDonVi}}</b></div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right"><b>Địa chỉ:</b></div>
                          <div class="col-sm-7">
                              <div ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhNew.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.diaChi}}</b></div>
                              <div ng-show="dbLenhNew.ID_TAIKHOAN_CHUYEN != dbLenhOld.ID_TAIKHOAN_CHUYEN"
                              style="color: red" ng-repeat="tkKBNN in listTKKBNNs" ng-if="tkKBNN.id == dbLenhOld.ID_TAIKHOAN_CHUYEN"><b>{{tkKBNN.diaChi}}</b></div>
                          </div>
                        </div>
                      </div>
                      
                  </div>
                </div>
                <div style="height: 15px;"></div>
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h2 class="panel-title">
                      Thông tin chi tiết
                    </h2>
                  </div>
                  <div class="panel-body" style="overflow: hidden;font-weight: bold;">
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Số lệnh</div>
                          <div class="col-sm-7">{{dbLenh.soLenh}}</div>
                        </div>
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Phụ lục hợp đồng</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">{{phuLuc.name}}</div>
                              <div ng-show="dbLenhNew.SO_LENH != dbLenhOld.SO_LENH" style="color: red"
                              ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG">{{phuLuc.name}}</div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Ngân hàng</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">
                                  {{phuLuc.maNHTM}}
                              </div>
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG && dbLenhNew.ID_PHULUC_HOPDONG != dbLenhOld.ID_PHULUC_HOPDONG" style="color: red">
                                  {{phuLuc.maNHTM}}
                              </div>
                          </div>
                        </div>
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Số tài khoản nhận</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">
                                  {{phuLuc.soTK}}
                              </div>
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG && dbLenhNew.ID_PHULUC_HOPDONG != dbLenhOld.ID_PHULUC_HOPDONG" style="color: red">
                                  {{phuLuc.soTK}}
                              </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Tên tài khoản</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">
                                  {{phuLuc.tenTKN}}
                              </div>
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG && dbLenhNew.ID_PHULUC_HOPDONG != dbLenhOld.ID_PHULUC_HOPDONG" style="color: red">
                                  {{phuLuc.tenTKN}}
                              </div>
                          </div>
                        </div>
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Nơi mở tài khoản</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">
                                  {{phuLuc.noiMoTKN}}
                              </div>
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG && dbLenhNew.ID_PHULUC_HOPDONG != dbLenhOld.ID_PHULUC_HOPDONG" style="color: red">
                                  {{phuLuc.noiMoTKN}}
                              </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Số tiền (Tỷ đồng)</div>
                          <div class="col-sm-7">
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhNew.ID_PHULUC_HOPDONG">
                                  {{phuLuc.soTien | dispNumber}}
                              </div>
                              <div ng-repeat="phuLuc in phuLucs" ng-if="phuLuc.id == dbLenhOld.ID_PHULUC_HOPDONG && dbLenhNew.ID_PHULUC_HOPDONG != dbLenhOld.ID_PHULUC_HOPDONG" style="color: red">
                                  {{phuLuc.soTien | dispNumber}}
                              </div>
                          </div>
                        </div>
                        <div class="form-group col-sm-6">
                          <div class="col-sm-5 right">Nội dung thanh toán</div>
                          <div class="col-sm-7">
                              <div>
                                  {{dbLenhNew.NDUNG_TTOAN}}
                              </div>
                              <div ng-if="dbLenhNew.NDUNG_TTOAN != dbLenhOld.NDUNG_TTOAN" style="color: red">
                                  {{dbLenhOld.NDUNG_TTOAN}}
                              </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        
                      </div>
                      
                    </div>  
                  </div>
            
                </form>
            </div>
         </div>
         <div class="form-group" ng-if="detailLogFlag">
            <div class="button-group center">
               <button type="button" class="btn btn-default accordion-toggle"
                       ng-click="hideDetail()">
                       <i class="glyphicon glyphicon-arrow-up"></i>Danh sách
               </button>
            </div>
          </div>
         </div>
        </form>
      </div>
   </div>
    
  </div> 
  <%@ include file="/includes/tpcp_bottom.jsp"%>