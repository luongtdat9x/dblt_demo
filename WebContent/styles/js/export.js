function exportToExcel(element, filename = '') {
    var htmls = "";
    var uri = 'data:application/vnd.ms-excel;base64,';
    var template =
        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    var base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };
    var format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
            return c[p];
        })
    };
    htmls = document.getElementById(element).innerHTML;
    var ctx = {
        worksheet: 'Worksheet',
        table: htmls
    }
    var link = document.createElement("a");
    link.download = filename ? filename + '.xls' : 'document.xls';
    var xxx = base64(format(template, ctx));
    console.log(xxx);
    link.href = uri + xxx;
    link.click();
}

function Export2Doc(element, filename = '') {
    var preHtml =
        "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title><style>@page WordSection1{size:29.7cm 21.0cm;mso-page-orientation:landscape;margin:2.0cm 2.0cm 2.0cm 3.0cm;mso-header-margin:36.0pt;mso-footer-margin:36.0pt;mso-paper-source:0;}div.WordSection1{page:WordSection1;}</style></head><body><div class='WordSection1'>";
    var postHtml = "</div></body></html>";
    var html = preHtml + document.getElementById(element).innerHTML + postHtml;
    var blob = new Blob(['\ufeff', html], {
        type: 'application/msword'
    });
    // Specify link url
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    // Specify file name
    filename = filename ? filename + '.doc' : 'document.doc';
    // Create download link element
    var downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);
    if (navigator.msSaveOrOpenBlob) {
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        // Create a link to the file
        downloadLink.href = url;
        // Setting the file name
        downloadLink.download = filename;
        //triggering the function
        downloadLink.click();
    }
    document.body.removeChild(downloadLink);
}

function Export2PdfApi(element, url, typeRpt, fileName) {
    var req = new XMLHttpRequest();
    req.open("POST", "https://api.html2pdf.app/v1/generate", true);
    req.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    var strHtml = "<html><body>" + document.getElementById(element).innerHTML + "<\/html><\/body>";
    strHtml = minify(strHtml);
    console.log(strHtml);
    var jsonBody = {
        html: strHtml,
        apiKey: "1fe8d9244e515b6370cf40f785f872cb7d3782a1918ea3e71d25386c1399ca0b",
        callBackUrl: "/report",
        landscape: true
    };
    req.send(JSON.stringify(jsonBody));
    req.addEventListener('load', function() {
        console.log(req.status);
        console.log(req.responseText);
    });
}

function submitSaveRpt(element, url, typeRpt, fileName) {
    var req = new XMLHttpRequest();
    req.open("POST", url + "?action=exReport", true);
    req.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    var strHtml = document.getElementById(element).innerHTML;
    var minify = require('html-minifier').minify;
    strHtml = minify(strHtml, {
        collapseWhitespace: true
    });
    console.log(strHtml);
    var jsonBody = {
        "strHtml": strHtml,
        "typeRpt": typeRpt
    };
    req.send(JSON.stringify(jsonBody));
    req.addEventListener('load', function() {
        console.log(req.status);
        var data = JSON.parse(req.responseText);
        console.log(data.strPdf);
        var a = document.createElement("a"); //Create <a>
        a.href = "data:application/pdf;base64," + data.strPdf; //Image Base64 Goes here
        a.download = fileName; //File name Here
        a.click();
    });
}

function Export2Pdf(element, filename = '') {
    var htmlContent = document.getElementById(element).innerHTML;
    htmlContent = htmlContent.replace(/.*table.*td.*/, "")
        .replace(/.*td.*tr.*td.*/g, "")
        .replace(/.*td.*table.*/g, "");
    var minify = require('html-minifier').minify;
    htmlContent = minify(htmlContent, {
        collapseWhitespace: true
    });
    console.log(htmlContent);
    var val = htmlToPdfmake(htmlContent);
    var dd = {
        info: {
            title: filename
        },
        pageSize: 'A4',
        pageOrientation: 'landscape',
        content: val
    };
    pdfMake.createPdf(dd).open();
};

function Export2PdfImage(element, filename = '') {
    var htmlContent = document.getElementById(element);
    var opt = {
        margin: 1,
        filename: filename + '.pdf',
        image: {
            type: 'jpeg',
            quality: 1
        },
        html2canvas: {
            scale: 5
        },
        jsPDF: {
            unit: 'in',
            format: 'A4',
            orientation: 'landscape'
        }
    };
    // New Promise-based usage:
    html2pdf().set(opt).from(htmlContent).save();
};