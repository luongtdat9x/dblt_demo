<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0034)http://tcs.kbnn.vn/Load_ChungTu.do -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>  
<HTML>
<HEAD>
  <TITLE>HỆ THỐNG QUẢN LÝ NGÂN QUỸ - ĐĂNG NHẬP HỆ THỐNG</TITLE>
  <META content="text/html; charset=UTF-8" http-equiv="Content-Type"></META>
  <LINK rel="stylesheet" type="text/css" href="styles/jquery-ui-1.11.3/jquery-ui.css" media="screen"/>
    <LINK rel="stylesheet" type="text/css" href="styles/css/khobac.css" media="screen"/>
    <LINK rel="stylesheet" type="text/css" href="styles/css/style.css" media="screen"/>    
    <LINK rel="stylesheet" type="text/css" href="styles/css/style1.css" media="screen"/>    
    <SCRIPT type="text/javascript" src="styles/js/jquery/respond.src.js"></SCRIPT>
    <LINK rel="stylesheet" type="text/css" href="styles/css/oracle.css" media="screen"/> 
    <LINK rel="stylesheet" type="text/css" href="styles/css/login.css" media="screen"/>  
    <link rel="stylesheet" type="text/css" href="styles/js/calendar/blue.css"/>
    <SCRIPT type="text/javascript" src="styles/js/moment.js"></SCRIPT>    
    <META content="no-cache" http-equiv="Pragma"></META>
    <META content="-1" http-equiv="Expires"></META>
    <META name="GENERATOR" content="MSHTML 8.00.7600.16821"></META>    
     <script src="styles/js/jquery/jquery-1.11.3.min.js"></script> 

    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <script src="styles/js/jquery/bootstrap.js"></script>
    <script src="styles/js/jquery/bootstrap-select.js"></script>
    <script src="styles/jquery-ui-1.11.3/jquery-ui.js"></script>
	<LINK rel="stylesheet" type="text/css" href="styles/css/bootstrap.min.css" media="screen"/>  
  <LINK rel="stylesheet" type="text/css" href="styles/css/showLoading.css" media="screen"/>  

    <!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="styles/js/jquery/selectivizr.js"></script>
  <noscript><link rel="stylesheet" href="[fallback css]" /></noscript>
  <![endif]-->
  <META content=no-cache http-equiv=Pragma>
  <META content=-1 http-equiv=Expires>
  <META name=GENERATOR content="MSHTML 8.00.7600.16821"> 
  
   <script type="text/javascript">
    function sbLogin() {
      var frm = document.forms[0];
      //set cookie
      setValueToCookie('domain', document.getElementById("domain_id").value);
      setValueToCookie('user_name_ttsp', document.getElementById("username_id").value);
      frm.action = 'loginAction';
      frm.submit();
  }
   function changekey(event){
     event.keyCode=9;
     return event.keyCode;
   }
   function focu(){
     $("#password").focus();
   }
$("input").on("keydown", function(event) {
    if (event.which === 13 && !$(this).is("textarea, :button, :submit")) {
        event.stopPropagation();
        event.preventDefault();
        
        $(this)
            .nextAll("input:not(:disabled, [readonly='readonly'])")
            .first()
            .focus();
    }
});
   </script>

</HEAD>
<BODY class="body">
    <%
      if(request.getSession().getAttribute("ma_nsd") != null){
        response.sendRedirect(request.getContextPath()+"/loginAction");
      }
      String ipAddress  = request.getHeader("X-FORWARDED-FOR");  
      if(ipAddress == null)  
      {  
        ipAddress = request.getRemoteAddr();  
      }
    %>   
  <form method="post" action="loginAction" autocomplete="off" onsubmit="false">
<div class="aspNetHidden">
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPBSpWU0tleTphOGI5NzRmMS00ZGVmLTQwZDEtODY2Ny03MWY4OGFhMTUzYjBkyOoCGNJqeblRm2Q543s6mbexiLToly27RnmbIwUFE7Q=">
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<!--<script src="./Đăng nhập_files/WebResource.axd" type="text/javascript"></script>-->


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'form1';//]]>
</script>

<!--<script src="./Đăng nhập_files/WebResource(1).axd" type="text/javascript"></script>-->
<div class="aspNetHidden">

	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAS2tnBebPT68FLTMtVaqA5STmhgdQItTlEiuL0IbUPrzv5kkMzFhEqj3AhtMeDYGmd8g36yNdIfiqEdy+MGhifvmKj2lza84a9ebyvNI1A+8Bev+1+yXYGH6C8FMCuDJkY=">
</div>

        <div class="login-box"  style="width:30%; height:20%;margin-top:10%">
            <div id="div_login" class="div_login" >
                <div class="logo-login">
                    
                    <img alt="eoffice" src="styles/images/logokbnn.png" style="width: 50px" title="">
                </div>
                <div class="baner-text">
          <!--          <p class="a" style="margin-top:2px">KHO BẠC NHÀ NƯỚC</p>-->
                    <p class="text-logo ">
                    <font size="3">KHO BẠC NHÀ NƯỚC</font><br/>
                    <font size="4.8">HỆ THỐNG QUẢN LÝ NGÂN QUỸ</font>
                    
                    </p>
                </div>
            </div>
            <div class="login-box-body">
                <div>
                    <div id="SslWarning" style="color: red;">
                        
                    </div>
                    <table id="loginControl" cellspacing="0" cellpadding="0" style="width:100%;border-collapse:collapse;">
	<tbody>
  <tr>
  <td>
                 <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-compressed"></i></span>
            <select title="Domain" name="loginForm.domain" id="domain_id" onkeydown="if(event.keyCode==13) event.keyCode=9;" class="form-control">
              <option value="TW">TW</option>
              <option value="MB">MB</option>
              <option value="MN">MN</option>
            </select>
          </div>         
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" name="loginForm.ma_nsd"  class="form-control" onkeydown="if(event.keyCode==13){ focu(); };" 
            onkeypress="return blockKeySpecial1(event);"  id="username_id" value="" title="Mã nhân viên" maxlength="20"/>
          </div>
          <br/>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="password" onkeydown="if(event.keyCode==13){ sbLogin(); };" 
                   class="form-control" onkeypress="capLock(event)" 
                   maxLength="20"  name="loginForm.mat_khau" id="password" value="" title="Mật khẩu"/>
          </div>
            <div class="login-button">
                    <input type="submit" name="loginControl$login" value="Đăng nhập" id="loginControl_login" class="btn btn-primary" onclick="sbLogin();">
             </div>
          <input type="hidden" name="loginForm.ip_truycap" value="<%=ipAddress%>"/>
          <input type="hidden" name="loginForm.mac_address" styleId="mac_address_id" value=""/>
          <input type="hidden" name="loginForm.user_may_truycap" styleId="user_may_truycap_id" value=""/>
          <input type="hidden" name="loginForm.ten_may_truycap" styleId="ten_may_truycap_id" value=""/>
          <input type="hidden" name="loginForm.max_number" styleId="max_number" value=""/>
          </td>
	</tr>
</tbody>
</table>
    <div class="btn-dangnhap"  onclick="sbLogin();">
            <span class=sortKey></span>
          </div >
                </div>
				<div style="color: red;text-align: justify;">
             <div class="app_error" style="margin-top:40px;">
               <s:if test="hasActionErrors()">
                   <div class="app_error_list">
                     <font size="4" color="Red"><b> Cảnh báo:</b> </font><s:actionerror/>
                    </div>
                </s:if>
              </div>
				</div>
            </div>
			
            <div class="login-footer">
                <p class="sologin">Bản quyền thuộc về Kho Bạc Nhà nước Việt Nam</p>
                <p>Địa chỉ: 32 Cát Linh, Quận Đống Đa, Hà Nội, Việt Nam</p>
                
                <p></p>
		<input type="hidden" value="172.16.10.55">
            </div>
        </div>
    

<script type="text/javascript">
//<![CDATA[
var _fV4UI = true;
//]]>
</script>
</form>
  <script type="text/javascript">
  getValueFromCookie();
 // getClientInfo();

  //alert(domainCookie);
 

  function blockKeySpecial1(e) {
      var key;
      if (window.event)// IE
      {
          key = e.keyCode;
      }
      else if (e.which)// Netscape/Firefox/Opera
      {
          key = e.which;
      }
      if (key == 37 || key == 38 || key == 39 || key == 40 || key == 16 || key==13//arrow key
 || key == 35 || key == 36 || key == 9//home,end,tab
 || key == 46 || key == 8//del,insert,backspace
 || (key > 31 && key < 44) || (key > 57 && key < 65) || (key > 90 && key < 97) || (key > 123 && key < 127))//Cac ki tu dac biet
          event.returnValue = false;
      else {
          return textToUpper();
      }
  }
function textToUpper()
{
	var keycharUppercase;
	var keychar;
	if(window.event){ // IE
		var keynum = event.keyCode;
		keychar = String.fromCharCode(keynum);
		keycharUppercase = keychar.toUpperCase();
		event.keyCode = keycharUppercase.charCodeAt(0);
	}	
	return true;
}
  // Use this function to retrieve a cookie.
  function getCookie(name) {
      var cname = name + "=";
      var dc = document.cookie;
      if (dc.length > 0) {
          begin = dc.indexOf(cname);
          if (begin !=  - 1) {
              begin += cname.length;
              end = dc.indexOf(";", begin);
              if (end ==  - 1)
                  end = dc.length;
              return unescape(dc.substring(begin, end));
          }
      }
      return null;
  }

  // Use this function to save a cookie.
  function setCookie(name, value, expires) {
      document.cookie = name + "=" + escape(value) + "; path=/" + ((expires == null) ? "" : "; " + "expires=" + expires.toGMTString());
  }

  // Use this function to delete a cookie.
  function delCookie(name) {
      document.cookie = name + "=; expires=Thu, 01-Jan-70 00:00:01 GMT" + "; path=/";
  }

  // Function to retrieve form element's value.
  function getValueFromCookie() {
      var value = getCookie('domain');
      var user_value = getCookie('user_name_ttsp');

      if (user_value != null && user_value != 'null' && user_value != '' && value != null && value != 'null' && value != ''){
          document.getElementById('domain_id').value = value;
          document.getElementById('username_id').value = user_value;
          document.getElementById('password').focus();
      }else{
        if (user_value != null && user_value != 'null' && user_value != ''){
            document.getElementById('username_id').value = user_value;
            document.getElementById('domain_id').focus();
        }
        if (value != null && value != 'null' && value != ''){
            document.getElementById('domain_id').value = value;
            document.getElementById('username_id').focus();
        }
      }
  }

  // Function to save form element's value.
  function setValueToCookie(name, value) {
      setCookie(name, value, null);
  }
  //get MAC, OS USER, COMPUTER NAME
  function getClientInfo() {
      var wshell = new ActiveXObject("WScript.Shell");
      var user_may_truycap_value = wshell.ExpandEnvironmentStrings("%USERNAME%");
      document.getElementById('user_may_truycap_id').value = user_may_truycap_value;

      var network = new ActiveXObject('WScript.Network');
      var ten_may_truycap_value = network.computerName;
      document.getElementById('ten_may_truycap_id').value = ten_may_truycap_value;

      var locator = new ActiveXObject ("WbemScripting.SWbemLocator");
      var service = locator.ConnectServer(".");
      var properties = service.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True");
      var e = new Enumerator (properties);
      var mac_address_value = '|';
      for (;!e.atEnd();e.moveNext ())
      {
            var p = e.item ();
            mac_address_value = mac_address_value + '|' + p.MACAddress;
      }
      mac_address_value = mac_address_value + '|'
      document.getElementById('mac_address_id').value = mac_address_value;

  }

    
</script>
 <script>
$("input").each(function(){
   if($.trim($(this).val()) != '') {
      $(this).prev('label').css('opacity', '0');
  }
});
$('input')
    .focus(function() {
        $(this).prev('label').css('opacity', '0');
    })
    .blur(function() {
        if($.trim($(this).val()) == '') {
            $(this).prev('label').css('opacity', '1');
        }
    }); 
 </script>
 </body>
 </html>
