<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<jsp:include page="/includes/tpcp_header.jsp"></jsp:include>
<div id="content">
 <div>
  <div class="panel-heading border-bottom">
   <h1 class="panel-title">
    <strong>Trang chủ</strong>
   </h1>
  </div>
  <div class="app_error_list">
   <div uib-alert="uib-alert" ng-repeat="alert in listAlerts"
        ng-class="'alert-' + (alert.type || 'warning')"
        close="listCloseAlert($index)">{{alert.errCode}} : {{alert.errMessage}}</div>
  </div>
  
  <div class="panel panel-default" style="min-height:300px;">
  
  </div>
  
 </div>
</div>
<%@ include file="/includes/tpcp_bottom.jsp"%>